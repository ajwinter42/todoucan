#!/bin/bash

cd api

printf "API: DOWN DOCKER CONTAINERS...\n"
docker-compose down -v

printf "\n\nAPI: UP DOCKER CONTAINERS...\n"
docker-compose up -d

printf "\n\nAPI: INSTALL DEPENDENCIES...\n"
docker-compose exec app composer install

printf "\n\nAPI: INSTALL DEPENDENCIES...\n"
docker-compose exec app composer db:migrate --no-interaction

printf "\n\nAPI: INSTALL DEPENDENCIES...\n"
docker-compose exec app composer seed

printf "\n\nAPP: BOOT THE APPLICATION...\n"
cd ../app
sudo npx vue-cli-service serve
