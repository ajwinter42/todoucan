import HttpRequest from 'axios'

HttpRequest.defaults.baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8123/' : 'http://api.toucando.space';

const header = function () {
    return  {
        headers: {
            "X-Token": localStorage.getItem('user-token')
        }
    }
};

const handleError = function (callback) {
    return function (error) {
        if (typeof error.response === 'undefined') {
            callback(500, 'A network error occurred.')
        } else {
            callback(error.response.data.code, error.response.data.message);
        }
    }
};

const Repository = function () {
};

Repository.prototype = {
    register(username, password, passwordConfirmation, success, failure) {
        const http = HttpRequest.post('register', {
            username,
            password,
            'password-confirmation': passwordConfirmation,
        });

        http.then(success);
        http.catch(response => failure(response.data.code, response.data.message));
    },

    login(username, password, success, failure) {
        const http = HttpRequest.post('login', {username, password});

        http.then(function (response) {
            return success(response.data.data);
        });
        http.catch(handleError(failure));
    },

    createJob(job, list, success, failure) {
        const http = HttpRequest.post(
            'job/create',
            {
                'user-reference': localStorage.getItem('user.reference'),
                name: job.name,
                'job-list-reference': list === null ? null : list.reference,
                description: job.description,
            },
            header()
        );

        http.then(function (response) {
            return success(response.data.data.job);
        });
        http.catch(handleError(failure));
    },

    createJobList(list, success, failure) {
        const http = HttpRequest.post(
            'job-list/create',
            {
                'user-reference': localStorage.getItem('user.reference'),
                name: list.getName(),
            },
            header()
        );

        http.then(function (response) {
            return success(response.data.data.jobList);
        });
        http.catch(handleError(failure));
    },

    updateJob(job, success, failure) {
        const http = HttpRequest.patch(
            `job/${job.reference}/update`,
            {
                'user-reference': localStorage.getItem('user.reference'),
                name: job.name,
                description: job.description
            },
            header()
        );

        http.then(success);
        http.catch(handleError(failure));
    },

    archiveJob(job, success, failure) {
        const http = HttpRequest.patch(
            `job/${job.reference}/archive`,
            {
                'user-reference': localStorage.getItem('user.reference'),
            },
            header()
        );

        http.then(success);
        http.catch(failure);
    },

    fetchUserJobs(success, failure) {
        const userReference = localStorage.getItem('user.reference');

        const http = HttpRequest.get(
            `user/${userReference}/jobs`,
            header()
        );

        http.then(function (response) {
            return success(response.data.data);
        });

        http.catch(handleError(failure));
    },

    fetchUserJob(reference, success, failure) {
        const userReference = localStorage.getItem('user.reference');

        const http = HttpRequest.get(
            `user/${userReference}/job/${reference}`,
            header()
        );

        http.then(function (response) {
            return success(response.data.data);
        });
        http.then(response => failure(response.data.code, response.data.message));
    },

    fetchAllUsernames(success, failure) {
        const userReference = localStorage.getItem('user.reference');

        const http = HttpRequest.get(
            `/user/${userReference}/invitation/usernames`,
            header()
        );

        http.then(function (response) {
            return success(response.data.data.usernames);
        });
        http.catch(handleError(failure));
    },

    invite(references, message, item, success, failure) {
        const http = HttpRequest.post(
            'invitation/send',
            {
                'user-reference': localStorage.getItem('user.reference'),
                references: references,
                message: message,
                'item-reference': item.reference,
            },
            header()
        );

        http.then(success);
        http.catch(handleError(failure));
    },

    fetchNotifications(success, failure) {
        const userReference = localStorage.getItem('user.reference');

        const http = HttpRequest.get(`user/${userReference}/notifications`, header());

        http.then(function (response) {
            return success(response.data.data.notifications);
        });
        http.catch(handleError(failure));
    },

    acceptInvite(reference, success, failure) {
        const userReference = localStorage.getItem('user.reference');

        const http = HttpRequest.get(
            `user/${userReference}/invitation/${reference}/accept`,
            header()
        );

        http.then(success);
        http.catch(handleError(failure));
    },

    authorization(success, failure) {
        const http = HttpRequest.post('authorization', {
            'user-reference': localStorage.getItem('user.reference')
        }, header());

        http.then(success);
        http.catch(handleError(failure));
    }
};

export default Repository;
