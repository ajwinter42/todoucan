import JobListModel from '../../model/JobList';

const JobList = function (user, job) {
    this.userAdapter = user;
    this.jobAdapter  = job;
};

JobList.prototype = {
    userAdapter: null,

    jobAdapter: null,

    make(json) {
        return new JobListModel(
            json.reference,
            json.createdBy,
            json.name,
            this.jobAdapter.makeMultiple(json.jobs),
        );
    },

    makeMultiple(json) {
        return json.map(function (item) {
            return this.make(item);
        }.bind(this));
    },
};

export default JobList;
