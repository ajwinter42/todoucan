import JobModel from '../../model/Job';

const Job = function (userAdapter) {
    this.userAdapter = userAdapter;
};

Job.prototype = {
    userAdapter: null,

    make(json) {
        let status = 'Archived';

        switch (json.status) {
            case 0:
                status = 'Waiting';
                break;
            case 1:
                status = 'Archived';
        }

        return new JobModel(
            json.reference,
            json.name,
            json.description,
            this.userAdapter.make(json.createdBy),
            this.userAdapter.makeMultiple(json.assignees),
            status
        );
    },

    makeMultiple(json) {
        return json.map(function (item) {
            return this.make(item);
        }.bind(this));
    },
};

export default Job;
