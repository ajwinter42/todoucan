import UserModel from '../../model/User';

const User = function () {
};

User.prototype = {
    make(json) {
        return new UserModel(
            json.reference,
            json.username
        );
    },

    makeMultiple(json) {
        return json.map(function (item) {
            return this.make(item);
        }.bind(this));
    },
};

export default User;
