import Vue from 'vue'
import Router from 'vue-router'
import Repository from '../services/Repository'

const repository = new Repository();

Vue.use(Router);

let authenticated = false;

const authorization = function (to, from, next) {
    if (authenticated === false) {
        const success = function () {
            authenticated = true;
            next();
        };

        const failure = function (code) {
            if (code === 401) {
                localStorage.clear();
                next('/login');
            }
        };

        repository.authorization(success, failure);

        return;
    }

    next();
};

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/login',
            name: 'login',
            component: () => import('../views/Login'),
        },
        {
            path: '/register',
            name: 'register',
            component: () => import('../views/Register'),
        },
        {
            path: '/',
            name: 'landing-page',
            component: () => import('../views/JobList/Created'),
        },
        {
            path: '/jobs/created',
            name: 'user-jobs',
            component: () => import('../views/JobList/Created'),
            beforeEnter: authorization
        },
        {
            path: '/jobs/assigned',
            name: 'assigned-jobs',
            component: () => import('../views/JobList/Assigned'),
            beforeEnter: authorization
        },
        {
            path: '/job/create',
            name: 'job-create',
            component: () => import('../views/Create/Job'),
            beforeEnter: authorization
        },
        {
            path: '/job-list/create',
            name: 'job-list-create',
            component: () => import('../views/Create/JobList'),
            beforeEnter: authorization
        },
        {
            path: '/job/:jobReference',
            name: 'job-view',
            props: true,
            component: () => import('../views/JobPage'),
            beforeEnter: authorization
        },
        {
            path: '/invite',
            name: 'invite',
            component: () => import('../views/Invite'),
            beforeEnter: authorization
        },
    ]
});
