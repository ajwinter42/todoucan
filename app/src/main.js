import Vue from 'vue'
import injector from 'vue-inject'
import App from './App.vue'
import router from './router'
import {library} from '@fortawesome/fontawesome-svg-core'
import {
    faTrash,
    faArrowLeft,
    faBell,
    faPaperPlane,
    faCrow,
    faCogs,
    faSignOutAlt
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import VueToastr from '@deveodk/vue-toastr'
import VueTextareaAutosize from 'vue-textarea-autosize'
import Multiselect from 'vue-multiselect'
import '@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css'
import 'vue-multiselect/dist/vue-multiselect.min.css'
import './style.css'
import UserJsonAdapter from './services/json-adapter/User';
import JobJsonAdapter from './services/json-adapter/Job';
import JobListJsonAdapter from './services/json-adapter/JobList';
import Repository from './services/Repository';

library.add(faTrash);
library.add(faArrowLeft);
library.add(faBell);
library.add(faPaperPlane);
library.add(faCrow);
library.add(faCogs);
library.add(faSignOutAlt);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('multiselect', Multiselect);

Vue.use(VueTextareaAutosize);
Vue.use(VueToastr, {
    defaultPosition: 'toast-top-full-width',
    defaultType: 'info',
    defaultTimeout: 2000,
    className: ['toucan-toast'],
});

Vue.config.productionTip = false;

const userJsonAdapter    = new UserJsonAdapter();
const jobJsonAdapter     = new JobJsonAdapter(userJsonAdapter);
const jobListJsonAdapter = new JobListJsonAdapter(userJsonAdapter, jobJsonAdapter);
const repository         = new Repository();

injector.service('userJsonAdapter', function () {
    return userJsonAdapter
});
injector.service('jobJsonAdapter', function () {
    return jobJsonAdapter
});
injector.service('jobListJsonAdapter', function () {
    return jobListJsonAdapter
});
injector.service('repository', function () {
    return repository
});

Vue.use(injector);

new Vue({
    render: h => h(App),
    router,
}).$mount('#app');
