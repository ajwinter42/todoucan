const User = function (reference, username) {
    this.reference = reference;
    this.username  = username;
};

User.prototype = {
    reference: null,

    username: null,

    getReference() {
        return this.reference;
    },

    getUsername() {
        return this.username;
    },
};

export default User;
