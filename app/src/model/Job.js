const Job = function (reference, name, description, createdBy, assignees, status) {
    switch (status) {
        case 0:
            status = 'Archived';
            break;

        case 1:
            status = 'Waiting';
            break;
    }

    this.reference   = reference;
    this.name        = name;
    this.description = description;
    this.createdBy   = createdBy;
    this.assignees   = assignees;
    this.status      = status;
};

Job.prototype = {
    reference: null,

    name: null,

    description: null,

    createdBy: null,

    assignees: null,

    status: null,

    getReference() {
        return this.reference;
    },

    getName() {
        return this.name;
    },

    getDescription() {
        return this.description;
    },

    getCreatedBy() {
        return this.createdBy;
    },

    getAssignees() {
        return this.assignees;
    },

    getStatus() {
        return this.status;
    },
};

export default Job;
