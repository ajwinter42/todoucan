const JobList = function (reference, createdBy, name, jobs) {
    this.reference = reference;
    this.createdBy = createdBy;
    this.name      = name;
    this.jobs      = jobs;
};

JobList.prototype = {
    reference: null,

    name: null,

    createdBy: null,

    jobs: null,

    getReference() {
        return this.reference;
    },

    getName() {
        return this.name;
    },

    getCreatedBy() {
        return this.createdBy;
    },

    getJobs() {
        return this.jobs;
    },
};

export default JobList;
