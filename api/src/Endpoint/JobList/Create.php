<?php

declare(strict_types=1);

namespace Toucando\Endpoint\JobList;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\BadRequest;
use Toucando\Middleware\Error\Exception\InternalError;
use Toucando\Persistence\JobList;
use Toucando\Persistence\UserInterface;
use Toucando\Service\JsonAdapter\JobListInterface as JobListJsonAdapter;
use Toucando\Service\Repository\JobListInterface as JobListRepository;
use Toucando\Service\RespondInterface;
use Toucando\Service\UuidInterface;

final class Create
{
    private RespondInterface $respond;

    private UuidInterface $uuid;

    private JobListRepository $jobListRepository;

    private JobListJsonAdapter $jobListJsonAdapter;

    public function __construct(
        RespondInterface $respond,
        UuidInterface $uuid,
        JobListRepository $jobListRepository,
        JobListJsonAdapter $jobListJson
    ) {
        $this->jobListRepository = $jobListRepository;
        $this->respond = $respond;
        $this->uuid = $uuid;
        $this->jobListJsonAdapter = $jobListJson;
    }

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $body = $request->getParsedBody();

        $name = $body['name'] ?? '';

        if ($name === '') {
            throw new BadRequest(
                new Error(
                    'create-job',
                    'Name is mandatory',
                    'name in parsed body is missing'
                )
            );
        }

        /** @var UserInterface $createdBy */
        $createdBy = $request->getAttribute('logged-in-user');

        try {
            $jobList = new JobList($this->uuid->generate(), $createdBy, $name);

            $this->jobListRepository->persist($jobList);
        } catch (Throwable $exception) {
            throw new InternalError(
                new Error(
                    'create-job',
                    'Unable to persist job list',
                    'job list unable to persist'
                )
            );
        }

        return $this->respond->success([
            'jobList' => $this->jobListJsonAdapter->toJson($jobList)
        ]);
    }
}
