<?php

declare(strict_types=1);

namespace Toucando\Endpoint;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\BadRequest;
use Toucando\Middleware\Error\Exception\InternalError;
use Toucando\Persistence\Authentication;
use Toucando\Persistence\User;
use Toucando\Service\RespondInterface;
use Toucando\Service\UuidInterface;

final class Register
{
    private EntityManagerInterface $database;

    private RespondInterface $respond;

    private UuidInterface $uuid;

    public function __construct(EntityManagerInterface $database, RespondInterface $respond, UuidInterface $uuid)
    {
        $this->database = $database;
        $this->respond = $respond;
        $this->uuid = $uuid;
    }

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $body = $request->getParsedBody();

        $username = $body['username'] ?? '';
        $password = $body['password'] ?? '';
        $confirmation = $body['password-confirmation'] ?? '';

        if ($username === '') {
            throw new BadRequest(
                new Error(
                    'register',
                    'Username field must be set',
                    'username was not set'
                )
            );
        }

        if ($password === '') {
            throw new BadRequest(
                new Error(
                    'register',
                    'Password field must be set',
                    'password was not set'
                )
            );
        }

        if ($confirmation === '') {
            throw new BadRequest(
                new Error(
                    'register',
                    'Password confirmation field must be set',
                    'password confirmation was not set'
                )
            );
        }

        if ($password !== $confirmation) {
            throw new BadRequest(
                new Error(
                    'register',
                    'Password does not match confirmation',
                    'password string must be identical match to the confirmation string'
                )
            );
        }

        $password = password_hash($password, PASSWORD_BCRYPT);

        $existingUsername = $this->database
            ->getRepository(Authentication::class)
            ->findBy(['username' => $username]);

        if (count($existingUsername) > 0) {
            throw new BadRequest(
                new Error(
                    'register',
                    'Username already exists',
                    'user has used a username that is already in use by another user'
                )
            );
        }

        $user = new User($this->uuid->generate());
        $authentication = new Authentication($this->uuid->generate(), $username, $password, $user);

        try {
            $this->database->persist($user);
            $this->database->persist($authentication);
            $this->database->flush();
        } catch (Throwable $exception) {
            throw new InternalError(
                new Error(
                    'register',
                    'Failed to persist users account',
                    'failed to persist either user entity or/and authentication entity'
                )
            );
        }

        return $this->respond->success();
    }
}
