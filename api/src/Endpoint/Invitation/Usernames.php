<?php

declare(strict_types=1);

namespace Toucando\Endpoint\Invitation;

use Psr\Http\Message\ServerRequestInterface;
use Throwable;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\InternalError;
use Toucando\Persistence\UserInterface;
use Toucando\Service\Repository\UserInterface as UserRepository;
use Toucando\Service\RespondInterface;

final class Usernames
{
    private UserRepository $userRepository;

    private RespondInterface $respond;

    public function __construct(UserRepository $userRepository, RespondInterface $respond)
    {
        $this->userRepository = $userRepository;
        $this->respond = $respond;
    }

    public function __invoke(ServerRequestInterface $request)
    {
        /** @var UserInterface $user */
        $user = $request->getAttribute('logged-in-user');

        try {
            $usernames = $this->userRepository->fetchAllUsernames($user);
        } catch (Throwable $exception) {
            throw new InternalError(
                new Error(
                    'invitations-usernames',
                    'Unable to query for usernames',
                    'error occurred whilst querying the database for usernames relating to user'
                )
            );
        }

        return $this->respond->success([
            'usernames' => array_map(fn (array $data) => [
                'reference' => $data['id'],
                'username' => $data['username'],
            ], $usernames)
        ]);
    }
}
