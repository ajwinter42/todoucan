<?php

declare(strict_types=1);

namespace Toucando\Endpoint\Invitation;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;
use Toucando\Exception\InvalidValue;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\BadRequest;
use Toucando\Middleware\Error\Exception\InternalError;
use Toucando\Persistence\UserInterface;
use Toucando\Service\Repository\InvitationInterface as InvitationRepository;
use Toucando\Service\Repository\UserInterface as UserRepository;
use Toucando\Service\RespondInterface;
use Toucando\Value\Uuid;
use Toucando\Value\UuidInterface;

final class Send
{
    private RespondInterface $respond;

    private UserRepository $userRepository;

    private InvitationRepository $invitationRepository;

    public function __construct(
        RespondInterface $respond,
        UserRepository $userRepository,
        InvitationRepository $invitationRepository
    ) {
        $this->respond = $respond;
        $this->userRepository = $userRepository;
        $this->invitationRepository = $invitationRepository;
    }

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        /** @var UserInterface $loggedInUser */
        $loggedInUser = $request->getAttribute('logged-in-user');

        $body = $request->getParsedBody();

        $references = $body['references'] ?? null;

        if (empty($references)) {
            throw new BadRequest(
                new Error(
                    'invitation-send',
                    'No user references given',
                    'user references are needed to complete this request'
                )
            );
        }

        $message = $body['message'] ?? '';

        $toUuid = fn (string $reference): UuidInterface => new Uuid($reference);

        try {
            $reference = $toUuid($body['item-reference'] ?? '');

            $users = $this->userRepository->fetchByReferences(
                ...array_map($toUuid, $references)
            );
        } catch (InvalidValue $exception) {
            throw new BadRequest(
                new Error(
                    'invitation-send',
                    'Invalid reference',
                    'a reference was found not to be a valid uuid format'
                )
            );
        }

        try {
            $this->invitationRepository->send($loggedInUser, $message, $reference, ...$users);
        } catch (Throwable $exception) {
            throw new InternalError(
                new Error(
                    'invitation-send',
                    'Failed to persist an Invitation entity',
                    'invitation entity failed to persist'
                )
            );
        }

        return $this->respond->success();
    }
}
