<?php

declare(strict_types=1);

namespace Toucando\Endpoint\Invitation;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\InternalError;
use Toucando\Persistence\InvitationInterface;
use Toucando\Persistence\UserInterface;
use Toucando\Service\Repository\InvitationInterface as InvitationRepository;
use Toucando\Service\RespondInterface;

final class Accept
{
    private RespondInterface $respond;

    private InvitationRepository $invitationRepository;

    public function __construct(RespondInterface $respond, InvitationRepository $invitationRepository)
    {
        $this->respond = $respond;
        $this->invitationRepository = $invitationRepository;
    }

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        /** @var UserInterface $loggedInUser */
        $loggedInUser = $request->getAttribute('logged-in-user');

        /** @var InvitationInterface $invitation */
        $invitation = $request->getAttribute('invitation');

        try {
            $this->invitationRepository->accept($invitation, $loggedInUser);
        } catch (Throwable $exception) {
            throw new InternalError(
                new Error(
                    'invitation-accept',
                    'Failed to remove invite and attach user to job list',
                    'job list has failed to persist relationship to user or invitation has failed to be removed'
                )
            );
        }

        return $this->respond->success();
    }
}
