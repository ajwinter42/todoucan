<?php

declare(strict_types=1);

namespace Toucando\Endpoint;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Persistence\InvitationInterface as InvitationEntity;
use Toucando\Persistence\UserInterface as UserEntity;
use Toucando\Service\RespondInterface;

final class Notifications
{
    private RespondInterface $respond;

    public function __construct(RespondInterface $respond)
    {
        $this->respond = $respond;
    }

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        /** @var UserEntity $loggedInUser */
        $loggedInUser = $request->getAttribute('logged-in-user');

        return $this->respond->success([
            'notifications' => array_map(
                fn (InvitationEntity $invitation): array => [
                    'type' => 'invite',
                    'username' => $invitation->getSenderUsername(),
                    'inviteReference' => $invitation->getReference()->getRaw(),
                    'message' => $invitation->getMessage(),
                ],
                $loggedInUser->getInvitationsReceived()
            )
        ]);
    }
}
