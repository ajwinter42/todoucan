<?php

declare(strict_types=1);

namespace Toucando\Endpoint;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\InternalError;
use Toucando\Middleware\Error\Exception\Unauthorised;
use Toucando\Persistence\Authentication;
use Toucando\Persistence\AuthenticationInterface;
use Toucando\Service\RespondInterface;
use Toucando\Service\JsonAdapter\UserInterface;
use Toucando\Service\UuidInterface;

final class Authenticate
{
    private RespondInterface $respond;

    private UserInterface $responseAdapterUser;

    private UuidInterface $uuid;

    private EntityManagerInterface $database;

    public function __construct(
        RespondInterface $respond,
        UserInterface $responseAdapterUser,
        UuidInterface $uuid,
        EntityManagerInterface $database
    ) {
        $this->respond = $respond;
        $this->responseAdapterUser = $responseAdapterUser;
        $this->uuid = $uuid;
        $this->database = $database;
    }

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $body = $request->getParsedBody();

        if (!isset($body['username'], $body['password'])) {
            throw $this->errorUnauthorized('Invalid username or password');
        }

        $username = $body['username'];
        $password = $body['password'];

        /** @var AuthenticationInterface|null $authentication */
        $authentication = $this->database
            ->getRepository(Authentication::class)
            ->findOneBy(['username' => $username]);

        if ($authentication === null) {
            throw $this->errorUnauthorized('no authentication entity found');
        }

        $correctPassword = password_verify($password, $authentication->getPassword());

        if (!$correctPassword) {
            throw $this->errorUnauthorized('password is incorrect');
        }

        $token = $this->uuid->generate();

        $authentication->setCurrentToken($token);

        try {
            $this->database->persist($authentication);
            $this->database->flush();
        } catch (Throwable $exception) {
            throw new InternalError(
                new Error(
                    'authenticate',
                    'Unable to persist token',
                    'token failed to persist to database'
                )
            );
        }

        return $this->respond->success([
            'token' => $token->getRaw(),
            'user' => $this->responseAdapterUser->toJson($authentication->getUser()),
        ]);
    }

    private function errorUnauthorized(string $reason): Unauthorised
    {
        return new Unauthorised(
            'Authorization',
            new Error('authenticate', 'User is unauthorized', $reason)
        );
    }
}
