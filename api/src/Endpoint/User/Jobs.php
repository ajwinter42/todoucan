<?php

declare(strict_types=1);

namespace Toucando\Endpoint\User;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Persistence\UserInterface as UserEntity;
use Toucando\Service\JsonAdapter\JobInterface as JobJsonAdapter;
use Toucando\Service\JsonAdapter\JobListInterface as JobListJsonAdapter;
use Toucando\Service\Repository\JobInterface as JobRepository;
use Toucando\Service\Repository\JobListInterface as JobListRepository;
use Toucando\Service\RespondInterface;

final class Jobs
{
    private JobRepository $jobRepository;

    private JobListRepository $jobListRepository;

    private JobJsonAdapter $jobJsonAdapter;

    private JobListJsonAdapter $jobListJsonAdapter;

    private RespondInterface $respond;

    public function __construct(
        JobRepository $jobRepository,
        JobListRepository $jobListRepository,
        JobJsonAdapter $jobJsonAdapter,
        JobListJsonAdapter $jobListJsonAdapter,
        RespondInterface $respond
    ) {
        $this->jobRepository = $jobRepository;
        $this->jobListRepository = $jobListRepository;
        $this->jobJsonAdapter = $jobJsonAdapter;
        $this->jobListJsonAdapter = $jobListJsonAdapter;
        $this->respond = $respond;
    }

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        /** @var UserEntity $loggedInUser */
        $loggedInUser = $request->getAttribute('logged-in-user');

        $jobsByAssignee = $this->jobRepository->fetchUnlistedByAssignee($loggedInUser);
        $jobsCreatedBy = $this->jobRepository->fetchUnlistedByCreatedBy($loggedInUser);
        $jobListsByAssignee = $this->jobListRepository->fetchByAssignee($loggedInUser);
        $jobListsCreatedBy = $this->jobListRepository->fetchByCreatedBy($loggedInUser);

        return $this->respond->success([
            'jobsAssigned' => $this->jobJsonAdapter->multipleToJson(...$jobsByAssignee),
            'jobsCreated' => $this->jobJsonAdapter->multipleToJson(...$jobsCreatedBy),
            'jobListsAssigned' => $this->jobListJsonAdapter->multipleToJson(...$jobListsByAssignee),
            'jobListsCreated' => $this->jobListJsonAdapter->multipleToJson(...$jobListsCreatedBy),
        ]);
    }
}
