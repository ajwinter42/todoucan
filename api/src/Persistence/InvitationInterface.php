<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Toucando\Value\UuidInterface;

interface InvitationInterface
{
    public function getMessage(): string;

    public function getSender(): UserInterface;

    public function getReference(): UuidInterface;

    public function getInvitedEntity(): InvitationalInterface;

    public function getSenderUsername(): string;
}
