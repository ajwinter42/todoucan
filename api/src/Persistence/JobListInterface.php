<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Toucando\Value\UuidInterface;

interface JobListInterface extends InvitationalInterface
{
    public function getReference(): UuidInterface;

    public function addUser(UserInterface $user): void;

    public function getName(): string;

    /**
     * @return JobInterface[]
     */
    public function getJobs(): array;

    public function getCreatedBy(): UserInterface;

    public function addJob(JobInterface $job): void;
}
