<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Doctrine\ORM\Mapping as ORM;
use Toucando\Persistence\Traits\Id;
use Toucando\Value\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="invites", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
 *
 * @uses ORM\Table
 */
final class Invitation implements InvitationInterface
{
    use Id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="id")
     * @ORM\JoinColumn(name="sender_reference", referencedColumnName="id")
     */
    private UserInterface $sender;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="id")
     * @ORM\JoinColumn(name="invitee_reference", referencedColumnName="id")
     */
    private UserInterface $invitee;

    /**
     * @ORM\Column(name="message", type="text")
     */
    private string $message;

    /**
     * @ORM\OneToOne(targetEntity="JobList")
     * @ORM\JoinColumn(name="job_list_reference", referencedColumnName="id")
     */
    private JobListInterface $jobList;

    /**
     * @ORM\OneToOne(targetEntity="Job")
     * @ORM\JoinColumn(name="job_reference", referencedColumnName="id")
     */
    private JobInterface $job;

    /**
     * @ORM\Column(name="seen", type="boolean")
     */
    private int $seen = 0;

    public function __construct(
        UuidInterface $reference,
        UserInterface $sender,
        UserInterface $invitee,
        InvitationalInterface $invitational,
        string $message = ''
    ) {
        $this->id = $reference->getRaw();
        $this->sender = $sender;
        $this->invitee = $invitee;
        $this->jobList = $invitational instanceof JobListInterface ? $invitational : null;
        $this->job = $invitational instanceof JobInterface ? $invitational : null;
        $this->message = $message;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getSender(): UserInterface
    {
        return $this->sender;
    }

    public function getInvitedEntity(): InvitationalInterface
    {
        return $this->job ?? $this->jobList;
    }

    public function getSenderUsername(): string
    {
        return $this->getSender()->getAuthentication()->getUsername();
    }
}
