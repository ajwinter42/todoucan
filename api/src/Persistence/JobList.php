<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Toucando\Persistence\Traits\Id;
use Toucando\Value\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="job_lists", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
 *
 * @uses ORM\Table
 */
class JobList implements JobListInterface
{
    use Id;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="jobLists")
     */
    private ArrayCollection $users;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="jobLists", cascade={"persist"})
     * @ORM\JoinColumn(name="created_by_reference", referencedColumnName="id")
     */
    private UserInterface $createdBy;

    /**
     * @ORM\OneToMany(targetEntity="Job", mappedBy="jobList")
     */
    private ArrayCollection $jobs;

    /**
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private string $name;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="assignedJobLists")
     */
    private ArrayCollection $assignees;

    public function __construct(UuidInterface $reference, UserInterface $createdBy, string $name)
    {
        $this->id = $reference->getRaw();
        $this->createdBy = $createdBy;
        $this->name = $name;
        $this->users = new ArrayCollection();
        $this->jobs = new ArrayCollection();
    }

    public function addUser(UserInterface $user): void
    {
        $this->users->add($user);
    }

    public function addJob(JobInterface $job): void
    {
        $job->setList($this);
        $this->jobs->add($job);
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return JobInterface[]
     */
    public function getJobs(): array
    {
        return $this->jobs->getValues();
    }

    /**
     * @return UserInterface[]
     */
    public function getUsers(): array
    {
        return $this->users->getValues();
    }

    public function getCreatedBy(): UserInterface
    {
        return $this->createdBy;
    }
}
