<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Toucando\Value\UuidInterface as UuidValueInterface;

interface JobInterface extends InvitationalInterface
{
    public const STATUS_ARCHIVED = 1;
    public const STATUS_WAITING  = 0;
    public const STATUS_DEFAULT  = self::STATUS_WAITING;
    public const STATUS_TYPES    = [
        self::STATUS_ARCHIVED,
        self::STATUS_WAITING,
    ];

    public function getName(): string;

    public function getDescription(): string;

    public function getImageFilename(): ?string;

    public function getCreatedBy(): UserInterface;

    public function getStatus(): int;

    public function getReference(): UuidValueInterface;

    /**
     * @return UserInterface[]
     */
    public function getAssignees(): array;

    public function archive(): void;

    public function setName(string $name): void;

    public function setDescription(string $description): void;

    public function setStatus(int $status): void;

    public function setList(JobListInterface $list): void;

    public function addAssignee(UserInterface $assignee): void;
}
