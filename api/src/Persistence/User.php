<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Toucando\Persistence\Traits\Id;
use Toucando\Value\UuidInterface as UuidValueInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="users", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
 *
 * @uses ORM\Table
 */
class User implements UserInterface
{
    use Id;

    /**
     * @ORM\OneToMany(targetEntity="Job", mappedBy="createdBy")
     */
    private ArrayCollection $jobs;

    /**
     * @ORM\OneToOne(targetEntity="Authentication", mappedBy="user")
     */
    private AuthenticationInterface $authentication;

    /**
     * @ORM\OneToMany(targetEntity="Invitation", mappedBy="sender")
     */
    private ArrayCollection $invitationsSent;

    /**
     * @ORM\OneToMany(targetEntity="Invitation", mappedBy="invitee")
     */
    private ArrayCollection $invitationsReceived;

    /**
     * @ORM\ManyToMany(targetEntity="JobList", inversedBy="assignees")
     * @ORM\JoinTable(name="assignees_job_lists")
     */
    private ArrayCollection $assignedJobLists;

    /**
     * @ORM\ManyToMany(targetEntity="Job", inversedBy="assignees")
     * @ORM\JoinTable(name="assignees")
     */
    private ArrayCollection $assignedJobs;

    /**
     * @ORM\OneToMany(targetEntity="JobList", mappedBy="createdBy")
     */
    private ArrayCollection $createdJobLists;

    public function __construct(UuidValueInterface $reference)
    {
        $this->id = $reference->getRaw();
        $this->jobs = new ArrayCollection();
        $this->invitationsSent = new ArrayCollection();
        $this->invitationsReceived = new ArrayCollection();
        $this->assignedJobLists = new ArrayCollection();
        $this->assignedJobs = new ArrayCollection();
        $this->createdJobLists = new ArrayCollection();
    }

    /**
     * @return InvitationInterface[]
     */
    public function getInvitationsReceived(): array
    {
        return $this->invitationsReceived->getValues();
    }

    public function getAuthentication(): AuthenticationInterface
    {
        return $this->authentication;
    }

    /**
     * @return JobListInterface[]
     */
    public function getCreatedJobLists(): array
    {
        return $this->createdJobLists->getValues();
    }

    public function assignJob(JobInterface $job): void
    {
        $this->assignedJobs->add($job);
    }

    public function assignJobList(JobListInterface $jobList): void
    {
        $this->assignedJobLists->add($jobList);
    }

    /**
     * @return JobListInterface[]
     */
    public function getAssignedJobLists(): array
    {
        return $this->assignedJobLists->getValues();
    }
}
