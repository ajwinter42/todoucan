<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Toucando\Value\UuidInterface as UuidValue;

interface AuthenticationInterface
{
    public function getUsername(): string;

    public function getPassword(): string;

    public function setCurrentToken(UuidValue $token): void;

    public function getUser(): UserInterface;
}
