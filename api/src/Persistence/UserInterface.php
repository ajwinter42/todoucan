<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Toucando\Value\UuidInterface as UuidValueInterface;

interface UserInterface
{
    public function getReference(): UuidValueInterface;

    /**
     * @return InvitationInterface[]
     */
    public function getInvitationsReceived(): array;

    public function getAuthentication(): AuthenticationInterface;

    /**
     * @return JobListInterface[]
     */
    public function getCreatedJobLists(): array;

    /**
     * @return JobListInterface[]
     */
    public function getAssignedJobLists(): array;

    public function assignJob(JobInterface $job): void;

    public function assignJobList(JobListInterface $jobList): void;
}
