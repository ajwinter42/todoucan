<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Doctrine\ORM\Mapping as ORM;
use Toucando\Persistence\Traits\Id;
use Toucando\Value\UuidInterface as UuidValue;
use Toucando\Value\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="authentication", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
 *
 * @uses ORM\Table
 */
class Authentication implements AuthenticationInterface
{
    use Id;

    /**
     * @ORM\Column(name="username", type="string", nullable=false)
     */
    private string $username;

    /**
     * @ORM\Column(name="password", type="string", nullable=false)
     */
    private string $password;

    /**
     * @ORM\Column(name="current_token", type="string", nullable=true)
     */
    private string $currentToken;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="authentication")
     * @ORM\JoinColumn(name="user_reference", referencedColumnName="id")
     */
    private UserInterface $user;

    public function __construct(UuidInterface $reference, string $username, string $password, UserInterface $user)
    {
        $this->id = $reference->getRaw();
        $this->username = $username;
        $this->password = $password;
        $this->user = $user;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setCurrentToken(UuidValue $token): void
    {
        $this->currentToken = $token->getRaw();
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }
}
