<?php

declare(strict_types=1);

namespace Toucando\Persistence\Traits;

use Toucando\Value\Uuid;
use Toucando\Value\UuidInterface;

trait Id
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", length=36, nullable=false)
     */
    private string $id;

    public function getReference(): UuidInterface
    {
        return new Uuid($this->id);
    }
}
