<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Toucando\Persistence\Traits\Id;
use Toucando\Value\UuidInterface as UuidValueInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="jobs", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
 *
 * @uses ORM\Table
 */
class Job implements JobInterface
{
    use Id;

    /**
     * @ORM\Column(name="name", type="string")
     */
    private string $name;

    /**
     * @ORM\Column(name="description", type="text")
     */
    private string $description;

    /**
     * @ORM\Column(name="image_filename", type="string", nullable=true)
     */
    private string $imageFilename;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="jobs")
     * @ORM\JoinColumn(name="created_by_reference", referencedColumnName="id")
     */
    private UserInterface $createdBy;

    /**
     * @ORM\Column(name="status", type="integer")
     */
    private int $status;


    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="assignedJobs")
     */
    private ArrayCollection $assignees;

    /**
     * @ORM\ManyToOne(targetEntity="JobList", inversedBy="jobs")
     * @ORM\JoinColumn(name="job_list_reference", referencedColumnName="id")
     */
    private JobListInterface $jobList;

    public function __construct(
        UuidValueInterface $reference,
        UserInterface $createdBy,
        string $name,
        string $description = '',
        string $imageFilename = ''
    ) {
        $this->id = $reference->getRaw();
        $this->name = $name;
        $this->description = $description;
        $this->imageFilename = $imageFilename;
        $this->createdBy = $createdBy;
        $this->status = self::STATUS_DEFAULT;
        $this->assignees = new ArrayCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getImageFilename(): ?string
    {
        return $this->imageFilename;
    }

    public function getCreatedBy(): UserInterface
    {
        return $this->createdBy;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return UserInterface[]
     */
    public function getAssignees(): array
    {
        return $this->assignees->getValues();
    }

    public function archive(): void
    {
        $this->status = self::STATUS_ARCHIVED;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function setList(JobListInterface $list): void
    {
        $this->jobList = $list;
    }

    public function addAssignee(UserInterface $assignee): void
    {
        $this->assignees->add($assignee);
    }
}
