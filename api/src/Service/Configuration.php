<?php

declare(strict_types=1);

namespace Toucando\Service;

final class Configuration implements ConfigurationInterface
{
    private array $settings;

    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    public function getDatabaseDriver(): string
    {
        return $this->settings['connection']['driver'] ?? '';
    }

    public function getDatabaseHost(): string
    {
        return $this->settings['connection']['driver'] ?? '';
    }

    public function getDatabasePort(): string
    {
        return $this->settings['connection']['driver'] ?? '';
    }

    public function getDatabaseName(): string
    {
        return $this->settings['connection']['driver'] ?? '';
    }

    public function getDatabaseUser(): string
    {
        return $this->settings['connection']['driver'] ?? '';
    }

    public function getDatabasePassword(): string
    {
        return $this->settings['connection']['driver'] ?? '';
    }

    public function getCorsOrigin(): array
    {
        return $this->settings['cors']['origin'] ?? [];
    }

    public function getCorsMethods(): array
    {
        return $this->settings['cors']['methods'] ?? [];
    }

    public function getCorsHeadersAllow(): array
    {
        return $this->settings['cors']['headers.allow'] ?? [];
    }

    public function getCorsHeadersExpose(): array
    {
        return $this->settings['cors']['headers.expose'] ?? [];
    }

    public function getCorsCredentials(): bool
    {
        return $this->settings['cors']['credentials'];
    }

    public function getCorsCache(): int
    {
        return $this->settings['cors']['cache'] ?? 0;
    }

    public function getEnvironmentName(): string
    {
        return $this->settings['environment-name'] ?? '';
    }

    public function getSentryDsn(): string
    {
        return $this->settings['sentry-dsn'] ?? '';
    }
}
