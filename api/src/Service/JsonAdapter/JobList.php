<?php

declare(strict_types=1);

namespace Toucando\Service\JsonAdapter;

use Toucando\Persistence\JobListInterface as EntityInterface;

final class JobList implements JobListInterface
{
    private JobInterface $jobAdapter;

    private UserInterface $userAdapter;

    public function __construct(JobInterface $jobAdapter, UserInterface $userAdapter)
    {
        $this->jobAdapter = $jobAdapter;
        $this->userAdapter = $userAdapter;
    }

    public function toJson(EntityInterface $jobList): array
    {
        return [
            'reference' => $jobList->getReference()->getRaw(),
            'name' => $jobList->getName(),
            'createdBy' => $this->userAdapter->toJson($jobList->getCreatedBy()),
            'jobs' => $this->jobAdapter->multipleToJson(...$jobList->getJobs()),
        ];
    }

    public function multipleToJson(EntityInterface ...$jobList): array
    {
        return array_map(
            fn (EntityInterface $jobList): array => $this->toJson($jobList),
            $jobList
        );
    }
}
