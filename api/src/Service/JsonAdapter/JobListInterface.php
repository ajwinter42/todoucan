<?php

declare(strict_types=1);

namespace Toucando\Service\JsonAdapter;

use Toucando\Persistence\JobListInterface as EntityInterface;

interface JobListInterface
{
    public function toJson(EntityInterface $jobList): array;

    public function multipleToJson(EntityInterface ...$jobLists): array;
}
