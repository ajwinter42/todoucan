<?php

declare(strict_types=1);

namespace Toucando\Service;

use Ramsey\Uuid\Uuid as Ramsey;
use Toucando\Value\Uuid as UuidValue;
use Toucando\Value\UuidInterface as UuidValueInterface;

final class Uuid implements UuidInterface
{
    public function generate(): UuidValueInterface
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return new UuidValue(
            Ramsey::uuid4()->serialize()
        );
    }
}
