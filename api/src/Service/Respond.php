<?php

declare(strict_types=1);

namespace Toucando\Service;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;

final class Respond implements RespondInterface
{
    private ResponseFactoryInterface $factory;

    public function __construct(ResponseFactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function success(array $data = []): ResponseInterface
    {
        $response = $this->factory->createResponse();

        $response->getBody()->write(
            json_encode([
                'code' => 200,
                'success' => true,
                'data' => $data,
            ])
        );

        return $response->withHeader('Content-Type', 'application/json');
    }
}
