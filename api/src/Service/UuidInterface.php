<?php

declare(strict_types=1);

namespace Toucando\Service;

use Toucando\Value\UuidInterface as UuidValue;

interface UuidInterface
{
    public function generate(): UuidValue;
}
