<?php

declare(strict_types=1);

namespace Toucando\Service\Repository;

use Toucando\Persistence\InvitationInterface as Entity;
use Toucando\Persistence\UserInterface;
use Toucando\Value\UuidInterface;

interface InvitationInterface
{
    public function fetchByReference(UuidInterface $uuid): ?Entity;

    public function accept(Entity $invitation, UserInterface $user): void;

    public function send(
        UserInterface $sender,
        string $message,
        UuidInterface $reference,
        UserInterface ...$users
    ): void;
}
