<?php

declare(strict_types=1);

namespace Toucando\Service\Repository;

use Toucando\Persistence\UserInterface as EntityInterface;
use Toucando\Value\Uuid;

interface UserInterface
{
    /**
     * @return EntityInterface[]
     */
    public function fetchByReferences(Uuid ...$references): array;

    public function fetchAllUsernames(EntityInterface $except): array;
}
