<?php

declare(strict_types=1);

namespace Toucando\Service\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Toucando\Persistence\Job as Entity;
use Toucando\Persistence\JobInterface as EntityInterface;
use Toucando\Persistence\UserInterface as UserEntity;
use Toucando\Value\UuidInterface;

final class Job implements JobInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function fetchByReference(UuidInterface $reference): ?EntityInterface
    {
        /** @var Entity|null $job */
        $job = $this->entityManager
            ->getRepository(Entity::class)
            ->find($reference->getRaw());

        return $job;
    }

    /**
     * @return Entity[]
     */
    public function fetchUnlistedByAssignee(UserEntity $user): array
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select('j')
            ->from(Entity::class, 'j')
            ->where(':reference MEMBER OF j.assignees')
            ->andWhere('j.jobList IS NULL')
            ->setParameter('reference', $user->getReference()->getRaw())
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Entity[]
     */
    public function fetchUnlistedByCreatedBy(UserEntity $user): array
    {
        return $this->entityManager
            ->getRepository(Entity::class)
            ->findBy([
                'createdBy' => $user->getReference()->getRaw(),
                'jobList' => null,
            ]);
    }

    public function persist(EntityInterface $job): void
    {
        $this->entityManager->persist($job);
        $this->entityManager->flush();
    }
}
