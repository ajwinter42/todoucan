<?php

declare(strict_types=1);

namespace Toucando\Service\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Throwable;
use Toucando\Persistence\Invitation as Entity;
use Toucando\Persistence\InvitationalInterface;
use Toucando\Persistence\InvitationInterface as EntityInterface;
use Toucando\Persistence\Job;
use Toucando\Persistence\JobInterface;
use Toucando\Persistence\JobList;
use Toucando\Persistence\JobListInterface;
use Toucando\Persistence\UserInterface;
use Toucando\Service\UuidInterface as UuidServiceInterface;
use Toucando\Value\UuidInterface;

final class Invitation implements InvitationInterface
{
    private EntityManagerInterface $entityManager;

    private UuidServiceInterface $uuid;

    public function __construct(EntityManagerInterface $entityManager, UuidServiceInterface $uuid)
    {
        $this->entityManager = $entityManager;
        $this->uuid = $uuid;
    }

    public function fetchByReference(UuidInterface $reference): ?EntityInterface
    {
        /** @var Entity|null $entity */
        $entity = $this->entityManager
            ->getRepository(Entity::class)
            ->find($reference->getRaw());

        return $entity;
    }

    /**
     * @throws Exception
     */
    public function accept(EntityInterface $invitation, UserInterface $user): void
    {
        $this->entityManager->beginTransaction();

        try {
            $invitational = $invitation->getInvitedEntity();

            switch (true) {
                case $invitational instanceof JobInterface:
                    $user->assignJob($invitational);
                    break;

                case $invitational instanceof JobListInterface:
                    $user->assignJobList($invitational);
                    foreach ($invitational->getJobs() as $job) {
                        $user->assignJob($job);
                    }
                    break;
            }

            $this->entityManager->remove($invitation);

            $this->entityManager->persist($user);

            $this->entityManager->flush();

            $this->entityManager->commit();
        } catch (Throwable $exception) {
            $this->entityManager->rollback();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Throwable
     */
    public function send(
        UserInterface $sender,
        string $message,
        UuidInterface $reference,
        UserInterface ...$users
    ): void {
        $this->entityManager->beginTransaction();

        /** @var InvitationalInterface $invitational */
        $invitational = $this->entityManager->getRepository(Job::class)->find($reference->getRaw())
            ?? $this->entityManager->getRepository(JobList::class)->find($reference->getRaw());

        try {
            foreach ($users as $user) {
                $this->entityManager->persist(
                    new Entity(
                        $this->uuid->generate(),
                        $sender,
                        $user,
                        $invitational,
                        $message
                    )
                );
            }

            $this->entityManager->flush();

            $this->entityManager->commit();
        } catch (Throwable $exception) {
            $this->entityManager->rollback();

            throw $exception;
        }
    }
}
