<?php

declare(strict_types=1);

namespace Toucando\Service\Repository;

use Toucando\Persistence\JobListInterface as EntityInterface;
use Toucando\Persistence\UserInterface;
use Toucando\Value\UuidInterface;

interface JobListInterface
{
    public function persist(EntityInterface $jobList): void;

    public function fetchByAssignee(UserInterface $assignee): array;

    public function fetchByCreatedBy(UserInterface $createdBy): array;

    public function fetchByReference(UuidInterface $reference): ?EntityInterface;
}
