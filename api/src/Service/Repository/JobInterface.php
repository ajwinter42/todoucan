<?php

declare(strict_types=1);

namespace Toucando\Service\Repository;

use Toucando\Persistence\JobInterface as Entity;
use Toucando\Persistence\UserInterface as UserEntity;
use Toucando\Value\UuidInterface;

interface JobInterface
{
    public function fetchByReference(UuidInterface $reference): ?Entity;

    public function persist(Entity $job): void;

    public function fetchUnlistedByAssignee(UserEntity $user): array;

    public function fetchUnlistedByCreatedBy(UserEntity $user): array;
}
