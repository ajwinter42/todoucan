<?php

declare(strict_types=1);

namespace Toucando\Service\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Toucando\Persistence\JobList as Entity;
use Toucando\Persistence\JobListInterface as EntityInterface;
use Toucando\Persistence\UserInterface;
use Toucando\Value\UuidInterface;

final class JobList implements JobListInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function persist(EntityInterface $jobList): void
    {
        $this->entityManager->persist($jobList);
        $this->entityManager->flush();
    }

    /**
     * @return EntityInterface[]
     */
    public function fetchByAssignee(UserInterface $user): array
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select('jl')
            ->from(Entity::class, 'jl')
            ->where(':reference MEMBER OF jl.assignees')
            ->setParameter('reference', $user->getReference()->getRaw())
            ->getQuery()
            ->getResult();
    }

    /**
     * @return EntityInterface[]
     */
    public function fetchByCreatedBy(UserInterface $user): array
    {
        return $this->entityManager
            ->getRepository(Entity::class)
            ->findBy([
                'createdBy' => $user->getReference()->getRaw(),
            ]);
    }

    public function fetchByReference(UuidInterface $reference): ?EntityInterface
    {
        /** @var EntityInterface|null $entity */
        $entity = $this->entityManager
            ->getRepository(Entity::class)
            ->find($reference->getRaw());

        return $entity;
    }
}
