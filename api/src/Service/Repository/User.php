<?php

declare(strict_types=1);

namespace Toucando\Service\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Toucando\Persistence\Authentication;
use Toucando\Persistence\User as Entity;
use Toucando\Persistence\UserInterface as EntityInterface;
use Toucando\Value\Uuid;

final class User implements UserInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return EntityInterface[]
     */
    public function fetchByReferences(Uuid ...$references): array
    {
        $references = array_map(
            fn (Uuid $uuid): string  => $uuid->getRaw(),
            $references
        );

        return $this->entityManager
            ->createQueryBuilder()
            ->select('u')
            ->from(Entity::class, 'u')
            ->where('u.id IN (:references)')
            ->setParameter('references', $references)
            ->getQuery()
            ->getResult();
    }

    public function fetchAllUsernames(EntityInterface $except): array
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select('a.username, u.id')
            ->from(Authentication::class, 'a')
            ->leftJoin('a.user', 'u')
            ->where('a.user != :userReference')
            ->setParameter('userReference', $except->getReference()->getRaw())
            ->getQuery()
            ->getArrayResult();
    }
}
