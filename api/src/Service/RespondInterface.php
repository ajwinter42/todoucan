<?php

declare(strict_types=1);

namespace Toucando\Service;

use Psr\Http\Message\ResponseInterface;

interface RespondInterface
{
    public function success(array $data = []): ResponseInterface;
}
