<?php

declare(strict_types=1);

namespace Toucando\Service;

interface ConfigurationInterface
{
    public function getDatabaseDriver(): string;

    public function getDatabaseHost(): string;

    public function getDatabasePort(): string;

    public function getDatabaseName(): string;

    public function getDatabaseUser(): string;

    public function getDatabasePassword(): string;

    public function getCorsOrigin(): array;

    public function getCorsMethods(): array;

    public function getCorsHeadersAllow(): array;

    public function getCorsHeadersExpose(): array;

    public function getCorsCredentials(): bool;

    public function getCorsCache(): int;

    public function getEnvironmentName(): string;

    public function getSentryDsn(): string;
}
