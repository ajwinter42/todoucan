<?php

declare(strict_types=1);

namespace Toucando;

use Doctrine\ORM\EntityManagerInterface;
use Pimple\Container as PimpleContainer;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Toucando\Service\ConfigurationInterface;
use Tuupola\Middleware\CorsMiddleware;

/**
 * ---------------------------------------------------------------------------------------------------------------------
 *                      PSR 15 compliant middleware
 * ---------------------------------------------------------------------------------------------------------------------
 *
 * @property Middleware\CheckToken                  $middlewareCheckToken
 * @property CorsMiddleware                         $middlewareCors
 * @property Middleware\Error\Psr15                 $middlewareError
 * @property Middleware\FetchAssignees              $middlewareFetchAssignees
 * @property Middleware\FetchJobList                $middlewareFetchJobList
 * @property Middleware\FetchUser                   $middlewareFetchLoggedInUser
 * @property Middleware\Jobs\Archive                $middlewareJobsArchive
 * @property Middleware\Jobs\Fetch\Single           $middlewareJobsFetchSingle
 * @property Middleware\Jobs\Hydrate                $middlewareJobsHydrate
 * @property Middleware\Jobs\Persist                $middlewareJobsPersist
 * @property Middleware\References                  $middlewareReferences
 * @property Middleware\RequestParse                $middlewareRequestParse
 * @property Middleware\Sentry                      $middlewareSentry
 * @property Middleware\Invitation\Find             $middlewareInvitationFind
 *
 *
 * ---------------------------------------------------------------------------------------------------------------------
 *                      Services
 * ---------------------------------------------------------------------------------------------------------------------
 * @property ConfigurationInterface                 $configuration
 * @property EntityManagerInterface                 $databaseTodoucan
 * @property Service\JsonAdapter\JobInterface       $jsonAdapterJob
 * @property Service\JsonAdapter\JobListInterface   $jsonAdapterJobList
 * @property Service\JsonAdapter\UserInterface      $jsonAdapterUser
 * @property Service\Repository\JobInterface        $repositoryJob
 * @property Service\Repository\JobListInterface    $repositoryJobList
 * @property Service\Repository\UserInterface       $repositoryUser
 * @property Service\Repository\InvitationInterface $repositoryInvitation
 * @property Service\RespondInterface               $responder
 * @property ResponseFactoryInterface               $responseFactory
 * @property Service\UuidInterface                  $uuidFactory
 */
final class Container implements ContainerInterface
{
    private PimpleContainer $pimple;

    public function __get($id)
    {
        return $this->pimple->offsetGet($id);
    }

    public function __set($id, $value)
    {
        $this->pimple->offsetSet($id, $value);
    }

    public function __isset($id)
    {
        return $this->has($id);
    }

    public function __construct(PimpleContainer $pimple)
    {
        $this->pimple = $pimple;
    }

    public function get($id)
    {
        return $this->pimple->offsetGet($id);
    }

    public function has($id): bool
    {
        return isset($this->pimple[$id]);
    }
}
