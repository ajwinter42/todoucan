<?php

declare(strict_types=1);

namespace Toucando\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Toucando\Service\Repository\UserInterface;
use Toucando\Value\Uuid;
use Toucando\Value\UuidInterface;

final class FetchAssignees implements MiddlewareInterface
{
    private UserInterface $userRepository;

    public function __construct(UserInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $body = $request->getParsedBody();

        $references = array_map(
            fn (string $reference): UuidInterface => new Uuid($reference),
            $body['assignee-references'] ?? []
        );

        $assignees = $this->userRepository->fetchByReferences(...$references);

        return $handler->handle(
            $request->withAttribute('assignees', $assignees)
        );
    }
}
