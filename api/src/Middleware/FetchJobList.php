<?php

declare(strict_types=1);

namespace Toucando\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Toucando\Persistence\JobListInterface;
use Toucando\Service\Repository\JobListInterface as JobListRepository;

final class FetchJobList implements MiddlewareInterface
{
    private JobListRepository $jobListRepository;

    public function __construct(JobListRepository $jobListRepository)
    {
        $this->jobListRepository = $jobListRepository;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $reference = $request->getAttribute('uuid-job-list-reference');

        $list = $reference ? $this->jobListRepository->fetchByReference($reference) : null;

        if ($list instanceof JobListInterface) {
            $request = $request->withAttribute('job-list', $list);
        }

        return $handler->handle($request);
    }
}
