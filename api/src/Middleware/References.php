<?php

declare(strict_types=1);

namespace Toucando\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Interfaces\RouteInterface;
use Throwable;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\BadRequest;
use Toucando\Value\Uuid;

final class References implements MiddlewareInterface
{
    /** @var string[] */
    private array $keys;

    public function __construct(string ...$keys)
    {
        $this->keys = $keys;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $body = $request->getParsedBody();

        /** @var RouteInterface $route */
        $route = $request->getAttribute('route');

        foreach ($this->keys as $key) {
            $raw = $body[$key] ?? $route->getArgument($key);

            if ($raw === null) {
                $request = $request->withAttribute($key, null);
                continue;
            }

            try {
                $reference = new Uuid($raw);
            } catch (Throwable $exception) {
                throw new BadRequest(
                    new Error(
                        'reference',
                        $exception->getMessage(),
                        $key . ' was not valid'
                    )
                );
            }

            $request = $request->withAttribute('uuid-' . $key, $reference);
        }

        return $handler->handle($request);
    }
}
