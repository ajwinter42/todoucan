<?php

declare(strict_types=1);

namespace Toucando\Middleware\Jobs;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\InternalError;
use Toucando\Persistence\JobInterface as JobEntity;
use Toucando\Persistence\JobListInterface;
use Toucando\Service\Repository\JobInterface;
use Toucando\Service\Repository\JobListInterface as JobListRepository;

final class Persist implements MiddlewareInterface
{
    private JobInterface $jobRepository;

    private JobListRepository $jobListRepository;

    public function __construct(JobInterface $jobRepository, JobListRepository $jobListRepository)
    {
        $this->jobRepository = $jobRepository;
        $this->jobListRepository = $jobListRepository;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        /** @var JobEntity $job */
        $job = $request->getAttribute('job');

        /** @var JobListInterface $list */
        $list = $request->getAttribute('job-list');

        try {
            $this->jobRepository->persist($job);

            if ($list instanceof JobListInterface) {
                $this->jobListRepository->persist($list);
            }
        } catch (Exception $exception) {
            throw new InternalError(
                new Error('job-persist', 'Failed to persist Job entity', 'job persistence failure')
            );
        }

        return $handler->handle($request);
    }
}
