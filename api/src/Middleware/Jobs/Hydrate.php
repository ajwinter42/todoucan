<?php

declare(strict_types=1);

namespace Toucando\Middleware\Jobs;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\BadRequest;
use Toucando\Persistence\Job;
use Toucando\Persistence\JobInterface;
use Toucando\Persistence\JobListInterface;
use Toucando\Persistence\UserInterface;
use Toucando\Service\UuidInterface;

final class Hydrate implements MiddlewareInterface
{
    public const NAME_IS_MANDATORY = 'name is mandatory can not be blank';

    private UuidInterface $uuid;

    public function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        /** @var UserInterface $createdBy */
        $createdBy = $request->getAttribute('logged-in-user');

        /** @var JobListInterface|null $list */
        $list = $request->getAttribute('job-list');

        $job = $request->getAttribute('job');

        $body = $request->getParsedBody();

        $name = $body['name'] ?? '';

        if ($name === '') {
            throw new BadRequest(
                new Error('hydrate-job', self::NAME_IS_MANDATORY, 'name is mandatory')
            );
        }

        $description = $body['description'] ?? '';
        $status = $body['status'] ?? JobInterface::STATUS_DEFAULT;

        if ($job instanceof JobInterface) {
            $job->setName($name);
            $job->setDescription($description);
            $job->setStatus($status);
        } else {
            $job = new Job(
                $this->uuid->generate(),
                $createdBy,
                $name,
                $description
            );
        }

        if ($list instanceOf JobListInterface) {
            $list->addJob($job);
        }

        return $handler->handle(
            $request->withAttribute('job', $job)
        );
    }
}
