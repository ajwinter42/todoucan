<?php

declare(strict_types=1);

namespace Toucando\Middleware\Jobs\Fetch;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\NotFound;
use Toucando\Persistence\JobInterface;
use Toucando\Service\Repository\JobInterface as JobRepository;

final class Single implements MiddlewareInterface
{
    public const JOB_NOT_FOUND = 'Job was not found';

    private JobRepository $jobRepository;

    public function __construct(JobRepository $jobRepository)
    {
        $this->jobRepository = $jobRepository;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $job = $this->jobRepository->fetchByReference(
            $request->getAttribute('uuid-job-reference')
        );

        if (!($job instanceof JobInterface)) {
            throw new NotFound(
                new Error('fetch-single-job', self::JOB_NOT_FOUND, 'job not found')
            );
        }

        return $handler->handle(
            $request->withAttribute('job', $job)
        );
    }
}
