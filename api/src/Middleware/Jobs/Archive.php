<?php

declare(strict_types=1);

namespace Toucando\Middleware\Jobs;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Toucando\Persistence\JobInterface as JobEntity;

final class Archive implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        /** @var JobEntity $job */
        $job = $request->getAttribute('job');

        $job->archive();

        return $handler->handle(
            $request->withAttribute('job', $job)
        );
    }
}
