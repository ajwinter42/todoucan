<?php

declare(strict_types=1);

namespace Toucando\Middleware;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\InternalError;
use Toucando\Middleware\Error\Exception\Unauthorised;
use Toucando\Persistence\Authentication;
use Toucando\Persistence\UserInterface as UserEntity;

final class CheckToken implements MiddlewareInterface
{
    private EntityManagerInterface $database;

    public function __construct(EntityManagerInterface $database)
    {
        $this->database = $database;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        /** @var UserEntity $loggedInUser */
        $loggedInUser = $request->getAttribute('logged-in-user');
        $token = $request->getHeader('X-Token');

        if (empty($token)) {
            throw new Unauthorised(
                'Authorization',
                new Error('check-token', 'User is unauthorized', 'users token was empty')
            );
        }

        try {
            $authentication = $this->database
                ->createQueryBuilder()
                ->select('count(a.id)')
                ->from(Authentication::class, 'a')
                ->leftJoin('a.user', 'u')
                ->where('a.currentToken = :token')
                ->andWhere('u.id = :userReference')
                ->setParameters([
                    'token' => $token,
                    'userReference' => $loggedInUser->getReference()->getRaw(),
                ])
                ->getQuery()
                ->getSingleScalarResult();
        } catch (Throwable $exception) {
            throw new InternalError(
                new Error(
                    'check-token',
                    'Failed to query database for token',
                    'querying the database for users token resulted in exception being thrown'
                )
            );
        }

        if ($authentication < 1) {
            throw new Unauthorised(
                'Authorization',
                new Error('check-token', 'User is unauthorized', 'users token was not found in storage')
            );
        }

        return $handler->handle($request);
    }
}
