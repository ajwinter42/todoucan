<?php

declare(strict_types=1);

namespace Toucando\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Interfaces\RouteInterface;
use Throwable;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\BadRequest;
use Toucando\Middleware\Error\Exception\NotFound;
use Toucando\Service\Repository\UserInterface as UserRepository;
use Toucando\Value\Uuid;

final class FetchUser implements MiddlewareInterface
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $body = $request->getParsedBody();

        /** @var RouteInterface $route */
        $route = $request->getAttribute('route');

        try {
            $reference = new Uuid(
                $body['user-reference'] ?? $route->getArgument('user-reference')
            );
        } catch (Throwable $exception) {
            throw new BadRequest(
                new Error(
                    'reference',
                    $exception->getMessage(),
                    'user-reference was not valid'
                )
            );
        }

        $user = $this->userRepository->fetchByReferences($reference);

        if (empty($user)) {
            throw new NotFound(
                new Error('fetch-user', 'User not found', 'fetch for user returned null')
            );
        }

        return $handler->handle(
            $request->withAttribute('logged-in-user', $user[0])
        );
    }
}
