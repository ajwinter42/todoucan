<?php

declare(strict_types=1);

namespace Toucando\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

use Throwable;
use function Sentry\{init, captureException};

final class Sentry implements MiddlewareInterface
{
    private string $environment;

    private string $dsn;

    private array $tags;

    public function __construct(string $environment, string $dsn, array $tags = [])
    {
        $this->environment = $environment;
        $this->dsn = $dsn;
        $this->tags = $tags;
    }

    /**
     * @throws Throwable
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $options = [
            'dsn' => $this->dsn,
            'environment' => $this->environment,
            'attach_stacktrace' => true,
            'tags' => $this->tags,
        ];

        try {
            init($options);

            return $handler->handle($request);
        } catch (Throwable $exception) {
            captureException($exception);

            throw $exception;
        }
    }
}
