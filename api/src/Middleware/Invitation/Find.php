<?php

declare(strict_types=1);

namespace Toucando\Middleware\Invitation;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\NotFound;
use Toucando\Service\Repository\InvitationInterface as InvitationRepository;

final class Find implements MiddlewareInterface
{
    private InvitationRepository $invitationRepository;

    public function __construct(InvitationRepository $invitationRepository)
    {
        $this->invitationRepository = $invitationRepository;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $invitation = $this->invitationRepository->fetchByReference(
            $request->getAttribute('uuid-invitation-reference')
        );

        if ($invitation === null) {
            throw new NotFound(
                new Error(
                    'invitation-accept',
                    'Invite not found',
                    'fetch invite using reference returned null'
                )
            );
        }

        return $handler->handle(
            $request->withAttribute('invitation', $invitation)
        );
    }
}
