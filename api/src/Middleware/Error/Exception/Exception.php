<?php

declare(strict_types=1);

namespace Toucando\Middleware\Error\Exception;

use Toucando\Middleware\Error\Error\Collection;

interface Exception
{
    public function getStatusCode(): int;

    public function getReasonPhrase(): string;

    public function getErrors(): Collection;

    public function getAdditionalHeaders(): array;
}
