<?php

declare(strict_types=1);

namespace Toucando\Middleware\Error\Exception;

use Toucando\Middleware\Error\Error\Collection;
use Toucando\Middleware\Error\Error\Error;

final class Unauthorised extends \DomainException implements Exception
{
    /** @var Collection */
    private $errors;

    /** @var string */
    private $authenticateHeader;

    public function __construct(string $authenticateHeader, Error $error, Error ...$errors)
    {
        parent::__construct($this->getReasonPhrase(), $this->getStatusCode());
        $this->errors             = new Collection($error, ...$errors);
        $this->authenticateHeader = $authenticateHeader;
    }

    public function getStatusCode(): int
    {
        return 401;
    }

    public function getReasonPhrase(): string
    {
        return 'Unauthorised';
    }

    public function getErrors(): Collection
    {
        return $this->errors;
    }

    public function getAdditionalHeaders(): array
    {
        return ['WWW-Authenticate' => $this->authenticateHeader];
    }
}
