<?php

declare(strict_types=1);

namespace Toucando\Middleware\Error\Exception;

use Toucando\Middleware\Error\Error\Collection;
use Toucando\Middleware\Error\Error\Error;

final class InternalError extends \DomainException implements Exception
{
    /** @var Collection */
    private $errors;

    public function __construct(Error $error, Error ...$errors)
    {
        parent::__construct($this->getReasonPhrase(), $this->getStatusCode());
        $this->errors = new Collection($error, ...$errors);
    }

    public function getStatusCode(): int
    {
        return 500;
    }

    public function getReasonPhrase(): string
    {
        return 'Internal Error';
    }

    public function getErrors(): Collection
    {
        return $this->errors;
    }

    public function getAdditionalHeaders(): array
    {
        return [];
    }
}
