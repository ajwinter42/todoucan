<?php

declare(strict_types=1);

namespace Toucando\Middleware\Error;

use RuntimeException;
use Toucando\Middleware\Error\Exception\Exception;
use Psr\Http\Message\ResponseFactoryInterface as Factory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class Psr15 implements MiddlewareInterface
{
    private Factory $factory;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    public function process(Request $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            return $handler->handle($request);
        } catch (Exception $e) {
            return $this->error($e);
        }
    }

    private function error(Exception $exception): ResponseInterface
    {
        $json = json_encode([
            'code' => $exception->getStatusCode(),
            'status' => $exception->getReasonPhrase(),
            'errors' => $exception->getErrors(),
        ]);

        if ($json === false) {
            throw new RuntimeException(json_last_error_msg(), json_last_error());
        }

        $response = $this->factory->createResponse($exception->getStatusCode(), $exception->getReasonPhrase());
        $response->getBody()->write($json);

        foreach ($exception->getAdditionalHeaders() as $key => $value) {
            $response = $response->withHeader($key, $value);
        }

        return $response->withHeader('Content-Type', 'application/json;charset=utf-8');
    }
}
