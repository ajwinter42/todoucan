<?php

declare(strict_types=1);

namespace Toucando\Middleware\Error\Error;

final class Error implements \JsonSerializable
{
    /** @var string */
    private $domain;

    /** @var string */
    private $message;

    /** @var string */
    private $reason;

    public function __construct(string $domain, string $message, string $reason)
    {
        $this->domain  = $domain;
        $this->message = $message;
        $this->reason  = $reason;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getReason(): string
    {
        return $this->reason;
    }

    public function jsonSerialize(): array
    {
        return [
            'domain'  => $this->domain,
            'message' => $this->message,
            'reason'  => $this->reason,
        ];
    }
}
