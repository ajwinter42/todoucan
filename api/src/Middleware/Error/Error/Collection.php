<?php

declare(strict_types=1);

namespace Toucando\Middleware\Error\Error;

use Toucando\Middleware\Error\Exception\InternalError;

final class Collection implements \Iterator, \Countable, \JsonSerializable
{
    /** @var Error[] */
    private $errors;

    /** @var int */
    private $position;

    public function __construct(Error $error, Error ...$errors)
    {
        $this->errors   = array_merge([$error], $errors);
        $this->position = 0;
    }

    /**
     * @throws InternalError
     */
    public function current(): Error
    {
        if ($this->valid()) {
            return $this->errors[$this->position];
        }

        throw new InternalError(
            new Error('errors', 'No current error', 'No current error')
        );
    }

    public function next(): void
    {
        ++$this->position;
    }

    public function key(): int
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return isset($this->errors[$this->position]);
    }

    public function rewind(): void
    {
        $this->position = 0;
    }

    public function count(): int
    {
        return count($this->errors);
    }

    public function jsonSerialize(): array
    {
        return $this->errors;
    }
}
