<?php

declare(strict_types=1);

namespace Toucando;

use Psr\Container\ContainerInterface;

final class App extends \Slim\App
{
    /**
     * @return Container
     */
    public function getContainer(): ?ContainerInterface
    {
        return parent::getContainer();
    }
}
