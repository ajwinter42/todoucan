<?php

declare(strict_types=1);

use Toucando\Service\JsonAdapter\Job;
use Toucando\Service\JsonAdapter\JobInterface;

$app->getContainer()->jsonAdapterJob = function () use ($app): JobInterface {
    return new Job(
        $app->getContainer()->jsonAdapterUser
    );
};
