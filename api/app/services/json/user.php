<?php

declare(strict_types=1);

use Toucando\Service\JsonAdapter\User;
use Toucando\Service\JsonAdapter\UserInterface;

$app->getContainer()->jsonAdapterUser = fn (): UserInterface => new User();
