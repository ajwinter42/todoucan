<?php

declare(strict_types=1);

use Toucando\Service\JsonAdapter\JobList;
use Toucando\Service\JsonAdapter\JobListInterface;

$app->getContainer()->jsonAdapterJobList = function () use ($app): JobListInterface {
    return new JobList(
        $app->getContainer()->jsonAdapterJob,
        $app->getContainer()->jsonAdapterUser
    );
};
