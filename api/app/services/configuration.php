<?php

declare(strict_types=1);

use Toucando\Service\Configuration;
use Toucando\Service\ConfigurationInterface;

$app->getContainer()->configuration = function (): ConfigurationInterface {
    return new Configuration(
        require __DIR__ . '/../settings.php'
    );
};
