<?php

declare(strict_types=1);

use Toucando\Service\Respond;
use Toucando\Service\RespondInterface;

$app->getContainer()->responder = function () use ($app): RespondInterface {
    return new Respond(
        $app->getContainer()->responseFactory
    );
};
