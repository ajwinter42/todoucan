<?php

declare(strict_types=1);

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Setup;

$app->getContainer()->databaseTodoucan = function () use ($app) : EntityManagerInterface {
    $configuration = $app->getContainer()->configuration;

    return EntityManager::create(
        [
            $configuration->getDatabaseDriver(),
            $configuration->getDatabaseHost(),
            $configuration->getDatabasePort(),
            $configuration->getDatabaseName(),
            $configuration->getDatabaseUser(),
            $configuration->getDatabaseUser(),
            $configuration->getDatabasePassword(),
        ],
        Setup::createAnnotationMetadataConfiguration(
            ['src/Persistence'],
            true,
            __DIR__ . '/../cache/proxies',
            null,
            false
        )
    );
};
