<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseFactoryInterface;
use Zend\Diactoros\ResponseFactory;

$app->getContainer()->responseFactory = function (): ResponseFactoryInterface {
    return new ResponseFactory();
};
