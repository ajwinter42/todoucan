<?php

declare(strict_types=1);

use Toucando\Service\Repository\JobList;
use Toucando\Service\Repository\JobListInterface;

$app->getContainer()->repositoryJobList = function () use ($app) : JobListInterface {
    return new JobList(
        $app->getContainer()->databaseTodoucan
    );
};
