<?php

declare(strict_types=1);

use Toucando\Service\Repository\User;
use Toucando\Service\Repository\UserInterface;

$app->getContainer()->repositoryUser = function () use ($app): UserInterface {
    return new User(
        $app->getContainer()->databaseTodoucan
    );
};
