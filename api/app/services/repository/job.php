<?php

declare(strict_types=1);

use Toucando\Service\Repository\Job;
use Toucando\Service\Repository\JobInterface;

$app->getContainer()->repositoryJob = function () use ($app): JobInterface {
    return new Job(
        $app->getContainer()->databaseTodoucan
    );
};
