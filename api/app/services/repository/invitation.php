<?php

declare(strict_types=1);

use Toucando\Service\Repository\Invitation;
use Toucando\Service\Repository\InvitationInterface;

$app->getContainer()->repositoryInvitation = function () use ($app): InvitationInterface {
    return new Invitation(
        $app->getContainer()->databaseTodoucan,
        $app->getContainer()->uuidFactory
    );
};
