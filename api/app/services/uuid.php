<?php

declare(strict_types=1);

use Toucando\Service\Uuid;

$app->getContainer()->uuidFactory = fn () => new Uuid();
