<?php

return [
    'connection' => [
        'driver' => 'pdo_pgsql',
        'host' => getenv('DB_HOST'),
        'port' => getenv('DB_PORT'),
        'dbname' => getenv('DB_NAME'),
        'user' => getenv('DB_USER'),
        'password' => getenv('DB_PASSWORD'),
    ],

    'cors' => [
        'origin' => ['*'],
        'methods' => ['GET', 'POST', 'PATCH', 'OPTIONS'],
        'headers.allow' => ['Content-Type', 'Accept', 'Origin', 'Authorization', 'X-Token'],
        'headers.expose' => [],
        'credentials' => true,
        'cache' => 0,
    ],

    'environment-name' => getenv('ENVIRONMENT_NAME') ?: '',

    'sentry-dsn' => 'https://203671ae7a354b5cb5a63b493de31494@sentry.io/1516411',
];
