<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Toucando\Middleware\Sentry;

$app->getContainer()->middlewareSentry = function () use ($app): callable {
    return function (ServerRequestInterface $request, RequestHandlerInterface $handler) use ($app): ResponseInterface {
        $middleware = new Sentry(
            $app->getContainer()->configuration->getEnvironmentName(),
            $app->getContainer()->configuration->getSentryDsn()
        );

        return $middleware->process($request, $handler);
    };
};
