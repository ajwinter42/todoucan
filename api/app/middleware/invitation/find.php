<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Toucando\Middleware\Invitation\Find;

$app->getContainer()->middlewareInvitationFind = function () use ($app): callable {
    return function (ServerRequestInterface $request, RequestHandlerInterface $handler) use ($app): ResponseInterface {
        $middleware = new Find(
            $app->getContainer()->repositoryInvitation
        );

        return $middleware->process($request, $handler);
    };
};
