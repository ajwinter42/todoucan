<?php

declare(strict_types=1);

use Psr\Http\Server\RequestHandlerInterface;
use Toucando\Middleware\Error\Psr15;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

$app->getContainer()->middlewareError = function () use ($app): callable {
    return function (ServerRequestInterface $request, RequestHandlerInterface $handler) use ($app): ResponseInterface {
        $middleware = new Psr15(
            $app->getContainer()->responseFactory
        );

        return $middleware->process($request, $handler);
    };
};
