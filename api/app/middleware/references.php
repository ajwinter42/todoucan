<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Toucando\Middleware\References;

$app->getContainer()->middlewareReferences = function (): callable {
    return fn (string ...$keys): callable => function (
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ) use ($keys) : ResponseInterface {
        $middleware = new References(...$keys);

        return $middleware->process($request, $handler);
    };
};
