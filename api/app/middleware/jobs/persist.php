<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Toucando\Middleware\Jobs\Persist;

$app->getContainer()->middlewareJobsPersist = function () use ($app) : callable {
    return function (ServerRequestInterface $request, RequestHandlerInterface $handler) use ($app) : ResponseInterface {
        $middleware = new Persist(
            $app->getContainer()->repositoryJob,
            $app->getContainer()->repositoryJobList
        );

        return $middleware->process($request, $handler);
    };
};
