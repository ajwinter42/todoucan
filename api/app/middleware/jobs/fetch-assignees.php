<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Toucando\Middleware\FetchAssignees;

$app->getContainer()->middlewareFetchAssignees = function () use ($app): callable {
    return function (ServerRequestInterface $request, RequestHandlerInterface $handler) use ($app): ResponseInterface {
        $middleware = new FetchAssignees(
            $app->getContainer()->repositoryUser
        );

        return $middleware->process($request, $handler);
    };
};
