<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Toucando\Middleware\Jobs\Hydrate;

$app->getContainer()->middlewareJobsHydrate = function () use ($app) : callable {
    return function (ServerRequestInterface $request, RequestHandlerInterface $handler) use ($app) : ResponseInterface {
        $middleware = new Hydrate(
            $app->getContainer()->uuidFactory
        );

        return $middleware->process($request, $handler);
    };
};
