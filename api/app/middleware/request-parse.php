<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Toucando\Middleware\RequestParse;

$app->getContainer()->middlewareRequestParse = function (): callable {
    return function (ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        $middleware = new RequestParse();

        return $middleware->process($request, $handler);
    };
};
