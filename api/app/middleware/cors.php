<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Tuupola\Middleware\CorsMiddleware;

$app->getContainer()->middlewareCors = function () use ($app): callable {
    return function (ServerRequestInterface $request, RequestHandlerInterface $handler) use ($app): ResponseInterface {
        $configuration = $app->getContainer()->configuration;

        $middleware = new CorsMiddleware([
            'origin' => $configuration->getCorsOrigin(),
            'methods' => $configuration->getCorsMethods(),
            'headers.allow' => $configuration->getCorsHeadersAllow(),
            'headers.expose' => $configuration->getCorsHeadersExpose(),
            'credentials' => $configuration->getCorsCredentials(),
            'cache' => $configuration->getCorsCache(),
        ]);

        return $middleware->process($request, $handler);
    };
};
