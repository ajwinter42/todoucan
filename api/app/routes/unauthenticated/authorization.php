<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;

$app
    ->post(
        '/authorization',
        function () use ($app) : ResponseInterface {
            return $app->getContainer()->responder->success();
        }
    )
    ->add($app->getContainer()->middlewareCheckToken)
    ->add($app->getContainer()->middlewareFetchLoggedInUser);
