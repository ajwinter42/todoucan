<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Endpoint\Register;

$app
    ->post(
        '/register',
        function (ServerRequestInterface $request) use ($app) : ResponseInterface {
            $endpoint = new Register(
                $app->getContainer()->databaseTodoucan,
                $app->getContainer()->responder,
                $app->getContainer()->uuidFactory
            );

            return $endpoint($request);
        }
    );
