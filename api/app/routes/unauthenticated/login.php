<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Endpoint\Authenticate;

$app
    ->post(
        '/login',
        function (ServerRequestInterface $request) use ($app) : ResponseInterface {
            $endpoint = new Authenticate(
                $app->getContainer()->responder,
                $app->getContainer()->jsonAdapterUser,
                $app->getContainer()->uuidFactory,
                $app->getContainer()->databaseTodoucan
            );

            return $endpoint($request);
        }
    );
