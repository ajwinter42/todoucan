<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Endpoint\Notifications;

$app
    ->get(
        '/user/{user-reference}/notifications',
        function (ServerRequestInterface $request) use ($app): ResponseInterface {
            $endpoint = new Notifications(
                $app->getContainer()->responder
            );

            return $endpoint($request);
        }
    )
    ->add(($app->getContainer()->middlewareReferences)('user-reference'));
