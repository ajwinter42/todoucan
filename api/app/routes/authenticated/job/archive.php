<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;

$app
    ->patch(
        '/job/{job-reference}/archive',
        function () use ($app) : ResponseInterface {
            return $app->getContainer()->responder->success();
        }
    )
    ->setName('job.archive')
    ->add($app->getContainer()->middlewareJobsPersist)
    ->add($app->getContainer()->middlewareJobsArchive)
    ->add($app->getContainer()->middlewareJobsFetchSingle)
    ->add(($app->getContainer()->middlewareReferences)('job-reference'));
