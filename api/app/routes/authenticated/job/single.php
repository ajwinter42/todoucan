<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Persistence\JobInterface as JobEntity;

$app
    ->get(
        '/user/{user-reference}/job/{job-reference}',
        function (ServerRequestInterface $request) use ($app): ResponseInterface {
            /** @var JobEntity $job */
            $job = $request->getAttribute('job');

            return $app->getContainer()->responder->success([
                'job' => $app->getContainer()->jsonAdapterJob->toJson($job)
            ]);
        }
    )
    ->setName('job.single')
    ->add($app->getContainer()->middlewareJobsFetchSingle)
    ->add(($app->getContainer()->middlewareReferences)('user-reference', 'job-reference'));
