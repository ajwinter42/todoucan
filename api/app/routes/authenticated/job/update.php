<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Persistence\JobInterface as JobEntity;

$app
    ->patch(
        '/job/{job-reference}/update',
        function (ServerRequestInterface $request) use ($app) : ResponseInterface {
            /** @var JobEntity $job */
            $job = $request->getAttribute('job');

            return $app->getContainer()->responder->success(
                $app->getContainer()->jsonAdapterJob->toJson($job)
            );
        }
    )
    ->setName('job.update')
    ->add($app->getContainer()->middlewareJobsPersist)
    ->add($app->getContainer()->middlewareJobsHydrate)
    ->add($app->getContainer()->middlewareJobsFetchSingle)
    ->add($app->getContainer()->middlewareFetchAssignees)
    ->add(($app->getContainer()->middlewareReferences)('job-reference'));
