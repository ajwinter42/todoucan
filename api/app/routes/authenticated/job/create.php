<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Persistence\JobInterface as JobEntity;

$app
    ->post(
        '/job/create',
        function (ServerRequestInterface $request) use ($app) : ResponseInterface {
            /** @var JobEntity $job */
            $job = $request->getAttribute('job');

            return $app->getContainer()->responder->success([
                'job' => $app->getContainer()->jsonAdapterJob->toJson($job)
            ]);
        }
    )
    ->setName('job.create')
    ->add($app->getContainer()->middlewareJobsPersist)
    ->add($app->getContainer()->middlewareJobsHydrate)
    ->add($app->getContainer()->middlewareFetchAssignees)
    ->add($app->getContainer()->middlewareFetchJobList)
    ->add(($app->getContainer()->middlewareReferences)('job-list-reference'));
