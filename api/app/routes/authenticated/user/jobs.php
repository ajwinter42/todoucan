<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Endpoint\User\Jobs;

$app
    ->get(
        '/user/{user-reference}/jobs',
        function (ServerRequestInterface $request) use ($app) : ResponseInterface {
            $endpoint = new Jobs(
                $app->getContainer()->repositoryJob,
                $app->getContainer()->repositoryJobList,
                $app->getContainer()->jsonAdapterJob,
                $app->getContainer()->jsonAdapterJobList,
                $app->getContainer()->responder
            );

            return $endpoint($request);
        }
    )
    ->setName('user.jobs')
    ->add(($app->getContainer()->middlewareReferences)('user-reference'));
