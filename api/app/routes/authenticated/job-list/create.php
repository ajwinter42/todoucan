<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Endpoint\JobList\Create;

$app->post(
    '/job-list/create',
    function (ServerRequestInterface $request) use ($app) : ResponseInterface {
        $endpoint = new Create(
            $app->getContainer()->responder,
            $app->getContainer()->uuidFactory,
            $app->getContainer()->repositoryJobList,
            $app->getContainer()->jsonAdapterJobList
        );

        return $endpoint($request);
    }
);
