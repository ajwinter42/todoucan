<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Endpoint\Invitation\Usernames;

$app
    ->get(
        '/user/{user-reference}/invitation/usernames',
        function (ServerRequestInterface $request) use ($app) : ResponseInterface {
            $endpoint = new Usernames(
                $app->getContainer()->repositoryUser,
                $app->getContainer()->responder
            );

            return $endpoint($request);
        }
    )
    ->add(($app->getContainer()->middlewareReferences)('user-reference'));
