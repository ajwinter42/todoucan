<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Endpoint\Invitation\Send;

$app
    ->post(
        '/invitation/send',
        function (ServerRequestInterface $request) use ($app) : ResponseInterface {
            $endpoint = new Send(
                $app->getContainer()->responder,
                $app->getContainer()->repositoryUser,
                $app->getContainer()->repositoryInvitation
            );

            return $endpoint($request);
        }
    );
