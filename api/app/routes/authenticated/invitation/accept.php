<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Endpoint\Invitation\Accept;

$app
    ->get(
        '/user/{user-reference}/invitation/{invitation-reference}/accept',
        function (ServerRequestInterface $request) use ($app) : ResponseInterface {
            $endpoint = new Accept(
                $app->getContainer()->responder,
                $app->getContainer()->repositoryInvitation
            );

            return $endpoint($request);
        }
    )
    ->add($app->getContainer()->middlewareInvitationFind)
    ->add(($app->getContainer()->middlewareReferences)('user-reference', 'invitation-reference'));
