<?php

declare(strict_types=1);

use Pimple\Container as PimpleContainer;
use Toucando\App;
use Toucando\Container;
use Zend\Diactoros\ResponseFactory;

require_once __DIR__ . '/../vendor/autoload.php';

$bootstrap = function (): App {
    date_default_timezone_set('UTC');

    $app = new App(
        new ResponseFactory(),
        new Container(
            new PimpleContainer()
        )
    );

    $load = function (string $root) use (&$app) {
        $rii = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(realpath($root)));
        /** @var SplFileInfo $file */
        foreach ($rii as $file) {
            if ($file->isFile() && $file->getExtension() === 'php') {
                /** @noinspection PhpIncludeInspection */
                require_once $file->getPathname();
            }
        }
    };

    $load(__DIR__ . '/services');
    $load(__DIR__ . '/middleware');

    $load(__DIR__ . '/routes/unauthenticated');

    $app
        ->group('', function () use ($load) {
            $load(__DIR__ . '/routes/authenticated');
        })
        ->add($app->getContainer()->middlewareCheckToken)
        ->add($app->getContainer()->middlewareFetchLoggedInUser);

    $app
        ->add($app->getContainer()->middlewareSentry)
        ->add($app->getContainer()->middlewareError)
        ->add($app->getContainer()->middlewareRequestParse)
        ->add($app->getContainer()->middlewareCors);

    return $app;
};
