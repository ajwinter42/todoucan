<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191019112859 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE invites (id UUID NOT NULL, sender_reference UUID DEFAULT NULL, invitee_reference UUID DEFAULT NULL, job_list_reference UUID DEFAULT NULL, job_reference UUID DEFAULT NULL, message TEXT NOT NULL, seen BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_37E6A6CF4AAADE5 ON invites (sender_reference)');
        $this->addSql('CREATE INDEX IDX_37E6A6C2FD8907D ON invites (invitee_reference)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_37E6A6C711615BD ON invites (job_list_reference)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_37E6A6C6913A602 ON invites (job_reference)');
        $this->addSql('CREATE TABLE jobs (id UUID NOT NULL, created_by_reference UUID DEFAULT NULL, job_list_reference UUID DEFAULT NULL, name VARCHAR(255) NOT NULL, description TEXT NOT NULL, image_filename VARCHAR(255) DEFAULT NULL, status INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A8936DC5F4A64C7E ON jobs (created_by_reference)');
        $this->addSql('CREATE INDEX IDX_A8936DC5711615BD ON jobs (job_list_reference)');
        $this->addSql('CREATE TABLE job_lists (id UUID NOT NULL, created_by_reference UUID DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_97A22D5F4A64C7E ON job_lists (created_by_reference)');
        $this->addSql('CREATE TABLE users (id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE assignees_job_lists (user_id UUID NOT NULL, joblist_id UUID NOT NULL, PRIMARY KEY(user_id, joblist_id))');
        $this->addSql('CREATE INDEX IDX_6B64B1C6A76ED395 ON assignees_job_lists (user_id)');
        $this->addSql('CREATE INDEX IDX_6B64B1C69499DC89 ON assignees_job_lists (joblist_id)');
        $this->addSql('CREATE TABLE assignees (user_id UUID NOT NULL, job_id UUID NOT NULL, PRIMARY KEY(user_id, job_id))');
        $this->addSql('CREATE INDEX IDX_12C41EDCA76ED395 ON assignees (user_id)');
        $this->addSql('CREATE INDEX IDX_12C41EDCBE04EA9 ON assignees (job_id)');
        $this->addSql('CREATE TABLE authentication (id UUID NOT NULL, user_reference UUID DEFAULT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, current_token VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FEB4C9FDAE43C4F7 ON authentication (user_reference)');
        $this->addSql('ALTER TABLE invites ADD CONSTRAINT FK_37E6A6CF4AAADE5 FOREIGN KEY (sender_reference) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE invites ADD CONSTRAINT FK_37E6A6C2FD8907D FOREIGN KEY (invitee_reference) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE invites ADD CONSTRAINT FK_37E6A6C711615BD FOREIGN KEY (job_list_reference) REFERENCES job_lists (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE invites ADD CONSTRAINT FK_37E6A6C6913A602 FOREIGN KEY (job_reference) REFERENCES jobs (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE jobs ADD CONSTRAINT FK_A8936DC5F4A64C7E FOREIGN KEY (created_by_reference) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE jobs ADD CONSTRAINT FK_A8936DC5711615BD FOREIGN KEY (job_list_reference) REFERENCES job_lists (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE job_lists ADD CONSTRAINT FK_97A22D5F4A64C7E FOREIGN KEY (created_by_reference) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE assignees_job_lists ADD CONSTRAINT FK_6B64B1C6A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE assignees_job_lists ADD CONSTRAINT FK_6B64B1C69499DC89 FOREIGN KEY (joblist_id) REFERENCES job_lists (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE assignees ADD CONSTRAINT FK_12C41EDCA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE assignees ADD CONSTRAINT FK_12C41EDCBE04EA9 FOREIGN KEY (job_id) REFERENCES jobs (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE authentication ADD CONSTRAINT FK_FEB4C9FDAE43C4F7 FOREIGN KEY (user_reference) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE invites DROP CONSTRAINT FK_37E6A6C6913A602');
        $this->addSql('ALTER TABLE assignees DROP CONSTRAINT FK_12C41EDCBE04EA9');
        $this->addSql('ALTER TABLE invites DROP CONSTRAINT FK_37E6A6C711615BD');
        $this->addSql('ALTER TABLE jobs DROP CONSTRAINT FK_A8936DC5711615BD');
        $this->addSql('ALTER TABLE assignees_job_lists DROP CONSTRAINT FK_6B64B1C69499DC89');
        $this->addSql('ALTER TABLE invites DROP CONSTRAINT FK_37E6A6CF4AAADE5');
        $this->addSql('ALTER TABLE invites DROP CONSTRAINT FK_37E6A6C2FD8907D');
        $this->addSql('ALTER TABLE jobs DROP CONSTRAINT FK_A8936DC5F4A64C7E');
        $this->addSql('ALTER TABLE job_lists DROP CONSTRAINT FK_97A22D5F4A64C7E');
        $this->addSql('ALTER TABLE assignees_job_lists DROP CONSTRAINT FK_6B64B1C6A76ED395');
        $this->addSql('ALTER TABLE assignees DROP CONSTRAINT FK_12C41EDCA76ED395');
        $this->addSql('ALTER TABLE authentication DROP CONSTRAINT FK_FEB4C9FDAE43C4F7');
        $this->addSql('DROP TABLE invites');
        $this->addSql('DROP TABLE jobs');
        $this->addSql('DROP TABLE job_lists');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE assignees_job_lists');
        $this->addSql('DROP TABLE assignees');
        $this->addSql('DROP TABLE authentication');
    }
}
