<?php

use Phinx\Seed\AbstractSeed;
use Toucando\Persistence\Job;

class Basic extends AbstractSeed
{
    public function run(): void
    {
        $users = [
            [
                'id' => 'eaf2780a-9bc1-499b-ba60-9bb0e1bb16f3',
            ],
            [
                'id' => 'e03ffb09-3aaa-4311-b7e9-23c7762056ef',
            ],
        ];

        $authentications = [
            [
                'id' => 'dd9976ff-210c-40fb-b848-be6e584dacb3',
                'user_reference' => $users[0]['id'],
                'username' => 'alex',
                'password' => password_hash('alex', PASSWORD_BCRYPT),
            ],
            [
                'id' => '4e011bd2-04ab-47d7-8c2d-fc343193e6c1',
                'user_reference' => $users[1]['id'],
                'username' => 'lewis',
                'password' => password_hash('lewis', PASSWORD_BCRYPT),
            ],
        ];

        $jobLists = [
            [
                'id' => '3d8e5c8b-1ae3-4625-959d-ebaaf5180725',
                'created_by_reference' => $users[0]['id'],
                'name' => 'Job List 1',
            ]
        ];

        $jobs = [
            [
                'id' => '04c21244-d032-443e-a0e7-68dd80ec9ce1',
                'created_by_reference' => $users[0]['id'],
                'job_list_reference' => null,
                'name' => 'Job 1',
                'description' => 'Description for job 1',
                'status' => Job::STATUS_DEFAULT,
            ],
            [
                'id' => '75f1578a-4bc2-4c69-aa46-09706711567f',
                'created_by_reference' => $users[0]['id'],
                'job_list_reference' => $jobLists[0]['id'],
                'name' => 'Job 1',
                'description' => 'Description for job 1',
                'status' => Job::STATUS_DEFAULT,
            ],
        ];

        $this->query('TRUNCATE jobs CASCADE');
        $this->query('TRUNCATE job_lists CASCADE');
        $this->query('TRUNCATE authentication CASCADE');
        $this->query('TRUNCATE users CASCADE');

        $this->table('users')->insert($users)->save();
        $this->table('authentication')->insert($authentications)->save();
        $this->table('job_lists')->insert($jobLists)->save();
        $this->table('jobs')->insert($jobs)->save();
    }
}
