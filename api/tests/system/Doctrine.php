<?php

declare(strict_types=1);

namespace ToucandoTests\System;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;
use Dotenv\Dotenv;
use InvalidArgumentException;
use Toucando\Persistence\Authentication;
use Toucando\Persistence\Job;
use Toucando\Persistence\User;

trait Doctrine
{
    /** @var string[] */
    protected static array $allEntities = [
        Authentication::class,
        User::class,
        Job::class,
    ];

    private static EntityManagerInterface $entityManager;

    /**
     * @throws DBALException
     * @throws InvalidArgumentException
     * @throws ORMException
     */
    public static function setUpBeforeClass(): void
    {
        require __DIR__ . '/../../vendor/autoload.php';

        $environment = Dotenv::create(__DIR__ . '/../../');
        $environment->load();

        $settings = include __DIR__ . '/../../app/settings.php';
        $settings = $settings['doctrine'];

        self::$entityManager = EntityManager::create(
            $settings['connection'],
            Setup::createAnnotationMetadataConfiguration(
                $settings['meta']['entity_path'],
                $settings['meta']['auto_generate_proxies'],
                $settings['meta']['proxy_dir'],
                $settings['meta']['cache'],
                false
            )
        );

        static::clearDatabase();

        parent::setUpBeforeClass();
    }

    /**
     * @throws DBALException
     */
    protected function tearDown(): void
    {
        self::clearDatabase();

        parent::setUp();
    }

    /**
     * @throws DBALException
     */
    protected static function clearDatabase(): void
    {
        foreach (self::$allEntities as $class) {
            $entityManager = self::$entityManager;

            $connection = $entityManager->getConnection();
            $schema = $connection->getSchemaManager();

            foreach ($schema->listTableNames() as $tableName) {
                $connection->executeQuery(
                    "TRUNCATE {$tableName} CASCADE;"
                );
            }
        }
    }

    protected static function fetchByReference(string $class, string $reference)
    {
        return self::$entityManager
            ->getRepository($class)
            ->findOneBy([
                'id' => $reference
            ]);
    }
}
