<?php

declare(strict_types=1);

namespace ToucandoTests\System\Repository;

use PHPUnit\Framework\TestCase;
use Toucando\Persistence\JobList as Entity;
use Toucando\Persistence\JobListInterface;
use Toucando\Persistence\User;
use Toucando\Service\Repository\JobList;
use Toucando\Value\Uuid;
use ToucandoTests\System\Doctrine;

final class JobListTest extends TestCase
{
    use Doctrine;

    public function testPersist(): void
    {
        $sut = new JobList(self::$entityManager);

        $reference = 'ae6daa4b-0961-4171-8a2f-2de3a1f94c33';

        $jobList = new Entity(new Uuid($reference), new User(new Uuid($reference)), '123');

        $sut->persist($jobList);

        $persisted = self::fetchByReference(Entity::class, $reference);

        $this->assertInstanceOf(JobListInterface::class, $persisted);
    }
}
