<?php

declare(strict_types=1);

namespace ToucandoTests\System\Repository;

use PHPUnit\Framework\TestCase;
use Toucando\Persistence\User;
use Toucando\Persistence\UserInterface;
use Toucando\Service\Repository\User as UserRepository;
use Toucando\Value\Uuid;
use ToucandoTests\System\Doctrine;

final class UserTest extends TestCase
{
    use Doctrine;

    public function testCanFetchByMultipleReferences(): void
    {
        $uuidOne = new Uuid('38c3b5ba-9a71-4161-9b27-f11664d0c8f7');
        $uuidTwo = new Uuid('6bf377c8-830d-4bc2-9abf-808bc4697a30');

        $userOne = new User($uuidOne);
        $userTwo = new User($uuidTwo);

        self::$entityManager->persist($userOne);
        self::$entityManager->persist($userTwo);
        self::$entityManager->flush();

        $sut = new UserRepository(self::$entityManager);

        $result = $sut->fetchByReferences($uuidOne, $uuidTwo);

        $match = array_filter($result, function (UserInterface $user): bool {
            return in_array($user->getReference()->getRaw(), [
                '38c3b5ba-9a71-4161-9b27-f11664d0c8f7',
                '6bf377c8-830d-4bc2-9abf-808bc4697a30',
            ], true);
        });

        $this->assertCount(2, $match);
    }
}
