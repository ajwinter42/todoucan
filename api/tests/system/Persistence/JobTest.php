<?php

declare(strict_types=1);

namespace ToucandoTests\System\Persistence;

use PHPUnit\Framework\TestCase;
use Toucando\Persistence\Job;
use Toucando\Persistence\User;
use Toucando\Value\Uuid;
use ToucandoTests\System\Doctrine;

final class JobTest extends TestCase
{
    use Doctrine;

    public function testCanPersistAndRetrieveJobFromTheDatabase(): void
    {
        $createdBy = new User(new Uuid('b75302ca-ddf5-4b64-b901-308489380932'));
        $name = 'Job Aa';
        $reference = new Uuid('2d8042cf-5376-419a-8dfc-81d8a5b57601');
        $description = 'job description for Job A';
        $imageFilename = 'image.jpg';

        $sut = new Job($reference, $createdBy, $name, $description, $imageFilename);

        self::$entityManager->persist($createdBy);
        self::$entityManager->persist($sut);
        self::$entityManager->flush();

        /** @var Job $persisted */
        $persisted = self::fetchByReference(Job::class, $sut->getReference()->getRaw());

        $this->assertSame($createdBy->getReference()->getRaw(), $persisted->getCreatedBy()->getReference()->getRaw());
        $this->assertSame($name, $persisted->getName());
        $this->assertSame($reference->getRaw(), $persisted->getReference()->getRaw());
        $this->assertSame($description, $persisted->getDescription());
        $this->assertSame($imageFilename, $persisted->getImageFilename());
    }

    public function testUpdateJob(): void
    {
        $createdBy = new User(new Uuid('a75302ca-ddf5-4b64-b901-308489380932'));
        $name = 'Job A';
        $reference = new Uuid('1d8042cf-5376-419a-8dfc-81d8a5b57601');
        $description = 'job description for Job A';
        $imageFilename = 'image.jpg';

        $sut = new Job($reference, $createdBy, $name, $description, $imageFilename);

        self::$entityManager->persist($createdBy);
        self::$entityManager->persist($sut);
        self::$entityManager->flush();

        /** @var Job $persisted */
        $persisted = self::fetchByReference(Job::class, $sut->getReference()->getRaw());

        $updatedName = 'new name';
        $updatedDescription = 'new description';
        $updatedStatus = Job::STATUS_ARCHIVED;

        $persisted->setStatus($updatedStatus);
        $persisted->setName($updatedName);
        $persisted->setDescription($updatedDescription);

        self::$entityManager->persist($persisted);
        self::$entityManager->flush();

        /** @var Job $updated */
        $updated = self::fetchByReference(Job::class, $persisted->getReference()->getRaw());

        $this->assertSame($updatedName, $updated->getName());
        $this->assertSame($updatedDescription, $updated->getDescription());
        $this->assertSame($updatedStatus, $updated->getStatus());
    }

    public function testArchiveJobPersistsToDatabase(): void
    {
        $createdBy = new User(new Uuid('a75302ca-ddf5-4b64-b901-308489380932'));

        $sut = new Job(
            new Uuid('1d8042cf-5376-419a-8dfc-81d8a5b57601'),
            $createdBy,
            'Job A',
            'job description for Job A',
            'image.jpg'
        );

        self::$entityManager->persist($createdBy);
        self::$entityManager->persist($sut);
        self::$entityManager->flush();

        /** @var Job $persisted */
        $persisted = self::fetchByReference(Job::class, $sut->getReference()->getRaw());

        $persisted->archive();

        self::$entityManager->persist($persisted);
        self::$entityManager->flush();

        /** @var Job $updated */
        $updated = self::fetchByReference(Job::class, $persisted->getReference()->getRaw());

        $this->assertSame(Job::STATUS_ARCHIVED, $updated->getStatus());
    }
}
