<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Service;

use PHPUnit\Framework\TestCase;
use Toucando\Service\Respond;
use ToucandoTests\Unit\Mocks;

final class RespondTest extends TestCase
{
    use Mocks;

    public function testSuccessResponse(): void
    {
        // Given I have a factory
        $stream = $this->mockStream();
        $stream
            ->expects($this->once())
            ->method('write')
            ->with('{"code":200,"success":true,"data":{"test":"123"}}');

        $response = $this->mockResponse();
        $response
            ->expects($this->once())
            ->method('getBody')
            ->willReturn($stream);
        $response
            ->expects($this->once())
            ->method('withHeader')
            ->with('Content-Type', 'application/json')
            ->willReturn($this->mockResponse());

        $factory = $this->mockResponseFactory();
        $factory
            ->expects($this->once())
            ->method('createResponse')
            ->willReturn($response);

        // When I pass the factory to the service and call success
        $sut = new Respond($factory);

        $sut->success(['test' => '123']);

        // Then all is well
    }
}
