<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Service\JsonAdapter;

use PHPUnit\Framework\TestCase;
use ToucandoTests\Unit\Mocks;
use Toucando\Service\JsonAdapter\User;
use Toucando\Value\Uuid;

/**
 * @covers \Toucando\Service\JsonAdapter\User
 */
final class UserTest extends TestCase
{
    use Mocks;

    public function testAdaptOneUser(): void
    {
        $sut = new User();

        $reference = new Uuid('8e0de505-f706-4028-8c60-0ef20a57a815');
        $username  = 'alex';

        $authentication = $this->mockAuthenticationEntity();
        $authentication->expects($this->once())->method('getUsername')->willReturn($username);

        $user = $this->mockUserEntity();
        $user->expects($this->once())->method('getReference')->willReturn($reference);
        $user->expects($this->once())->method('getAuthentication')->willReturn($authentication);

        $array = $sut->toJson($user);

        $this->assertArrayHasKey('reference', $array);
        $this->assertArrayHasKey('username', $array);

        $this->assertSame($reference->getRaw(), $array['reference']);
        $this->assertSame($username, $array['username']);
    }

    public function testAdaptMultipleUsers(): void
    {
        $sut = new User();

        $referenceOne = new Uuid('8e0de505-f706-4028-8c60-0ef20a57a815');
        $referenceTwo = new Uuid('8e0de505-f706-4028-8c60-0ef20a57a812');

        $user = $this->mockUserEntity();
        $user->expects($this->once())->method('getReference')->willReturn($referenceOne);

        $userTwo = $this->mockUserEntity();
        $userTwo->expects($this->once())->method('getReference')->willReturn($referenceTwo);

        $array = $sut->multipleToJson($user, $userTwo);

        $this->assertCount(2, $array);

        $this->assertArrayHasKey('reference', $array[0]);

        $this->assertSame($referenceOne->getRaw(), $array[0]['reference']);

        $this->assertArrayHasKey('reference', $array[1]);

        $this->assertSame($referenceTwo->getRaw(), $array[1]['reference']);
    }
}
