<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Service\JsonAdapter;

use PHPUnit\Framework\TestCase;
use Toucando\Service\JsonAdapter\JobList;
use ToucandoTests\Unit\Mocks;

final class JobListTest extends TestCase
{
    use Mocks;

    public function testToJson(): void
    {
        // Given I have a job list entity
        $uuid = $this->mockUuidValue();
        $uuid->expects($this->once())->method('getRaw')->willReturn('uuid');

        $jobList = $this->mockJobListEntity();
        $jobList->expects($this->once())->method('getReference')->willReturn($uuid);
        $jobList->expects($this->once())->method('getName')->willReturn('name');
        $jobList->expects($this->once())->method('getJobs')->willReturn([]);

        $jobAdapter = $this->mockJobAdapter();
        $jobAdapter
            ->expects($this->once())
            ->method('multipleToJson')
            ->willReturn([]);

        $userAdapter = $this->mockUserJsonAdapter();
        $userAdapter
            ->expects($this->once())
            ->method('toJson')
            ->willReturn([]);

        // Given I pass the job list to the toJson method
        $sut = new JobList($jobAdapter, $userAdapter);

        $result = $sut->toJson($jobList);

        // Then I should get the expected array
        $this->assertSame(
            [
                'reference' => 'uuid',
                'name'      => 'name',
                'createdBy' => [],
                'jobs'      => [],
            ],
            $result
        );
    }
}
