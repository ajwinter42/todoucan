<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Middleware\Jobs\Fetch;

use PHPUnit\Framework\TestCase;
use Toucando\Middleware\Error\Exception\NotFound;
use Toucando\Middleware\Jobs\Fetch\Single;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;

final class SingleTest extends TestCase
{
    use Request, Mocks;

    public function testJobNotFoundThrowNotFoundError(): void
    {
        // Given the request has a reference that does not match
        $reference = $this->mockUuidValue();

        $request = $this->makePostRequest()->withAttribute('uuid-job-reference', $reference);

        $jobRepository = $this->mockJobRepository();
        $jobRepository
            ->expects($this->once())
            ->method('fetchByReference')
            ->with($reference)
            ->willReturn(null);

        // When I pass the request to the middleware and invoke it
        $sut = new Single($jobRepository);

        $this->expectException(NotFound::class);
        $sut->process($request, $this->makeRequestHandler());

        // Then an Error is thrown
    }
}
