<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Middleware\Jobs;

use Exception;
use PHPUnit\Framework\TestCase;
use Toucando\Middleware\Error\Exception\InternalError;
use Toucando\Middleware\Jobs\Persist;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;

final class PersistTest extends TestCase
{
    use Request, Mocks;

    public function testIfPersistFailedThrowInternalError(): void
    {
        // Given I have a request
        $job = $this->mockJobEntity();

        $request = $this->makePostRequest()->withAttribute('job', $job);

        $jobRepository = $this->mockJobRepository();
        $jobRepository
            ->expects($this->once())
            ->method('persist')
            ->with($job)
            ->willThrowException(new Exception());

        // When I invoke the middleware
        $sut = new Persist($jobRepository, $this->mockJobListRepository());

        $this->expectException(InternalError::class);
        $sut->process($request, $this->makeRequestHandler());

        // Then a Internal Error has been thrown
    }

    public function testIfJobIsPersistedThenRequestIsHandled(): void
    {
        // Given I have a request
        $job = $this->mockJobEntity();

        $jobRepository = $this->mockJobRepository();
        $jobRepository
            ->expects($this->once())
            ->method('persist')
            ->with($job);

        $request = $this->makePostRequest()->withAttribute('job', $job);

        // When I invoke the middleware
        $sut = new Persist($jobRepository, $this->mockJobListRepository());

        // Then all is fine when I process
        $sut->process($request, $this->makeRequestHandler());
    }

    public function testIfJobListIsInTheRequestThenRelateJobToJobList(): void
    {
        // Given I have a request
        $job = $this->mockJobEntity();
        $jobList = $this->mockJobListEntity();

        $jobRepository = $this->mockJobRepository();
        $jobRepository
            ->expects($this->once())
            ->method('persist')
            ->with($job);

        $jobListRepository = $this->mockJobListRepository();
        $jobListRepository
            ->expects($this->once())
            ->method('persist')
            ->with($jobList);

        $request = $this->makePostRequest()
            ->withAttribute('job', $job)
            ->withAttribute('job-list', $jobList);

        // When I invoke the middleware
        $sut = new Persist($jobRepository, $jobListRepository);

        // Then all is fine when I process
        $sut->process($request, $this->makeRequestHandler());
    }
}
