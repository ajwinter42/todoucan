<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Middleware\Jobs;

use PHPUnit\Framework\TestCase;
use Toucando\Middleware\Jobs\Archive;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;

final class ArchiveTest extends TestCase
{
    use Request, Mocks;

    public function testArchiveIsCalled(): void
    {
        // Given I have a job in the request
        $job = $this->mockJobEntity();
        $job->expects($this->once())->method('archive');

        $request = $this->makePostRequest()->withAttribute('job', $job);

        // When I pass the request to the middleware and invoke it
        $sut = new Archive();

        $sut->process($request, $this->makeRequestHandler());

        // Then all is well
    }
}
