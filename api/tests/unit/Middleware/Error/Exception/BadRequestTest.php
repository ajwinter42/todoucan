<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Middleware\Error\Exception;

use DomainException;
use PHPUnit\Framework\TestCase;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\Exception;
use Toucando\Middleware\Error\Exception\BadRequest;

/**
 * @covers \Toucando\Middleware\Error\Exception\BadRequest
 */
final class BadRequestTest extends TestCase
{
    public function testInterfaceIsImplemented(): void
    {
        $sut = new BadRequest(new Error('foo', 'bar', 'baz'));

        $this->assertInstanceOf(Exception::class, $sut);
    }

    public function testBaseExceptionIsExtended(): void
    {
        $sut = new BadRequest(new Error('foo', 'bar', 'baz'));

        $this->assertInstanceOf(DomainException::class, $sut);
    }

    public function testStatusCodeIs400(): void
    {
        $sut = new BadRequest(new Error('foo', 'bar', 'baz'));

        $this->assertSame(400, $sut->getStatusCode());
    }

    public function testGetReasonPhraseIsSet(): void
    {
        $sut = new BadRequest(new Error('foo', 'bar', 'baz'));

        $this->assertSame('Bad Request', $sut->getReasonPhrase());
    }

    public function testAdditionalHeadersAreEmpty(): void
    {
        $sut = new BadRequest(new Error('foo', 'bar', 'baz'));

        $this->assertSame([], $sut->getAdditionalHeaders());
    }

    public function testAllErrorsPassedToConstructorAreReturned(): void
    {
        $error1 = new Error('foo', 'bar', 'baz');
        $error2 = new Error('foo', 'bar', 'baz');
        $error3 = new Error('foo', 'bar', 'baz');

        $sut = new BadRequest($error1, $error2, $error3);

        $this->assertCount(3, $sut->getErrors());
        $this->assertContains($error1, $sut->getErrors());
        $this->assertContains($error2, $sut->getErrors());
        $this->assertContains($error3, $sut->getErrors());
    }

    public function testCallsParentConstruct(): void
    {
        $sut = new BadRequest(new Error('foo', 'bar', 'baz'));

        $this->assertSame($sut->getCode(), $sut->getStatusCode());
    }
}
