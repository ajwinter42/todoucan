<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Middleware\Error\Exception;

use DomainException;
use PHPUnit\Framework\TestCase;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\Exception;
use Toucando\Middleware\Error\Exception\InternalError;

/**
 * @covers \Toucando\Middleware\Error\Exception\InternalError
 */
final class InternalErrorTest extends TestCase
{
    public function testInterfaceIsImplemented(): void
    {
        $sut = new InternalError(new Error('foo', 'bar', 'baz'));

        $this->assertInstanceOf(Exception::class, $sut);
    }

    public function testBaseExceptionIsExtended(): void
    {
        $sut = new InternalError(new Error('foo', 'bar', 'baz'));

        $this->assertInstanceOf(DomainException::class, $sut);
    }

    public function testStatusCodeIs500(): void
    {
        $sut = new InternalError(new Error('foo', 'bar', 'baz'));

        $this->assertSame(500, $sut->getStatusCode());
    }

    public function testGetReasonPhraseIsSet(): void
    {
        $sut = new InternalError(new Error('foo', 'bar', 'baz'));

        $this->assertSame('Internal Error', $sut->getReasonPhrase());
    }

    public function testAdditionalHeadersAreEmpty(): void
    {
        $sut = new InternalError(new Error('foo', 'bar', 'baz'));

        $this->assertSame([], $sut->getAdditionalHeaders());
    }

    public function testAllErrorsPassedToConstructorAreReturned(): void
    {
        $error1 = new Error('foo', 'bar', 'baz');
        $error2 = new Error('foo', 'bar', 'baz');
        $error3 = new Error('foo', 'bar', 'baz');

        $sut = new InternalError($error1, $error2, $error3);

        $this->assertCount(3, $sut->getErrors());
        $this->assertContains($error1, $sut->getErrors());
        $this->assertContains($error2, $sut->getErrors());
        $this->assertContains($error3, $sut->getErrors());
    }

    public function testCallsParentConstruct(): void
    {
        $sut = new InternalError(new Error('foo', 'bar', 'baz'));

        $this->assertSame($sut->getCode(), $sut->getStatusCode());
    }
}
