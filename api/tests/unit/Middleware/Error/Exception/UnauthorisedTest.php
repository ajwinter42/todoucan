<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Middleware\Error\Exception;

use DomainException;
use PHPUnit\Framework\TestCase;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\Exception;
use Toucando\Middleware\Error\Exception\Unauthorised;

/**
 * @covers \Toucando\Middleware\Error\Exception\Unauthorised
 */
final class UnauthorisedTest extends TestCase
{
    public function testInterfaceIsImplemented(): void
    {
        $sut = new Unauthorised('Bearer', new Error('foo', 'bar', 'baz'));

        $this->assertInstanceOf(Exception::class, $sut);
    }

    public function testBaseExceptionIsExtended(): void
    {
        $sut = new Unauthorised('Bearer', new Error('foo', 'bar', 'baz'));

        $this->assertInstanceOf(DomainException::class, $sut);
    }

    public function testStatusCodeIs401(): void
    {
        $sut = new Unauthorised('Bearer', new Error('foo', 'bar', 'baz'));

        $this->assertSame(401, $sut->getStatusCode());
    }

    public function testGetReasonPhraseIsSet(): void
    {
        $sut = new Unauthorised('Bearer', new Error('foo', 'bar', 'baz'));

        $this->assertSame('Unauthorised', $sut->getReasonPhrase());
    }

    public function testAdditionalHeadersContainWWWAuthenticate(): void
    {
        $sut = new Unauthorised('Bearer', new Error('foo', 'bar', 'baz'));

        $this->assertSame(['WWW-Authenticate' => 'Bearer'], $sut->getAdditionalHeaders());

        $sut = new Unauthorised('Basic', new Error('foo', 'bar', 'baz'));

        $this->assertSame(['WWW-Authenticate' => 'Basic'], $sut->getAdditionalHeaders());
    }

    public function testAllErrorsPassedToConstructorAreReturned(): void
    {
        $error1 = new Error('foo', 'bar', 'baz');
        $error2 = new Error('foo', 'bar', 'baz');
        $error3 = new Error('foo', 'bar', 'baz');

        $sut = new Unauthorised('Bearer', $error1, $error2, $error3);

        $this->assertCount(3, $sut->getErrors());
        $this->assertContains($error1, $sut->getErrors());
        $this->assertContains($error2, $sut->getErrors());
        $this->assertContains($error3, $sut->getErrors());
    }

    public function testCallsParentConstruct(): void
    {
        $sut = new Unauthorised('Bearer', new Error('foo', 'bar', 'baz'));

        $this->assertSame($sut->getCode(), $sut->getStatusCode());
    }
}
