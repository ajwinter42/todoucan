<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Middleware\Error\Exception;

use DomainException;
use PHPUnit\Framework\TestCase;
use Toucando\Middleware\Error\Error\Error;
use Toucando\Middleware\Error\Exception\Exception;
use Toucando\Middleware\Error\Exception\NotFound;

/**
 * @covers \Toucando\Middleware\Error\Exception\NotFound
 */
final class NotFoundTest extends TestCase
{
    public function testInterfaceIsImplemented(): void
    {
        $sut = new NotFound(new Error('foo', 'bar', 'baz'));

        $this->assertInstanceOf(Exception::class, $sut);
    }

    public function testBaseExceptionIsExtended(): void
    {
        $sut = new NotFound(new Error('foo', 'bar', 'baz'));

        $this->assertInstanceOf(DomainException::class, $sut);
    }

    public function testStatusCodeIs404(): void
    {
        $sut = new NotFound(new Error('foo', 'bar', 'baz'));

        $this->assertSame(404, $sut->getStatusCode());
    }

    public function testGetReasonPhraseIsSet(): void
    {
        $sut = new NotFound(new Error('foo', 'bar', 'baz'));

        $this->assertSame('Not Found', $sut->getReasonPhrase());
    }

    public function testAdditionalHeadersAreEmpty(): void
    {
        $sut = new NotFound(new Error('foo', 'bar', 'baz'));

        $this->assertSame([], $sut->getAdditionalHeaders());
    }

    public function testAllErrorsPassedToConstructorAreReturned(): void
    {
        $error1 = new Error('foo', 'bar', 'baz');
        $error2 = new Error('foo', 'bar', 'baz');
        $error3 = new Error('foo', 'bar', 'baz');

        $sut = new NotFound($error1, $error2, $error3);

        $this->assertCount(3, $sut->getErrors());
        $this->assertContains($error1, $sut->getErrors());
        $this->assertContains($error2, $sut->getErrors());
        $this->assertContains($error3, $sut->getErrors());
    }

    public function testCallsParentConstruct(): void
    {
        $sut = new NotFound(new Error('foo', 'bar', 'baz'));

        $this->assertSame($sut->getCode(), $sut->getStatusCode());
    }
}
