<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Middleware\Error\Error;

use ArgumentCountError;
use Countable;
use Iterator;
use PHPUnit\Framework\TestCase;
use Toucando\Middleware\Error\Error\Collection;
use Toucando\Middleware\Error\Error\Error;
use TypeError;

/**
 * @covers \Toucando\Middleware\Error\Error\Collection
 */
final class CollectionTest extends TestCase
{
    public function testInterfacesAreImplemented(): void
    {
        $collection = new Collection($this->createError());

        $this->assertInstanceOf(Iterator::class, $collection);
        $this->assertInstanceOf(Countable::class, $collection);
    }

    public function testCannotCreateEmptyCollection(): void
    {
        $this->expectException(ArgumentCountError::class);

        /** @noinspection PhpParamsInspection */
        new Collection();
    }

    /**
     * @dataProvider provideInvalidCollectionConstructionValues
     *
     * @var mixed $data
     */
    public function testCollectionConstructionValue(array $data): void
    {
        $this->expectException(TypeError::class);

        new Collection(...$data);
    }

    public function provideInvalidCollectionConstructionValues(): array
    {
        return [
            [
                [
                    new class
                    {
                    }
                ]
            ],
            [['1']],
            [[2]],
            [[null]],
            [[[]]],
            [[true]],
            [[false]],
            [
                [
                    function () {
                    }
                ]
            ],
        ];
    }

    public function testCollectionIsCountable(): void
    {
        $collection = new Collection(
            $this->createError(),
            $this->createError(),
            $this->createError()
        );

        $this->assertSame(3, $collection->count());
    }

    public function testCollectionIsIterable(): void
    {
        $collection = new Collection(
            $this->createError(),
            $this->createError(),
            $this->createError()
        );

        foreach ($collection as $error) {
            $this->assertInstanceOf(Error::class, $error);
        }
    }

    public function testAuditsAreReturnedInTheOrderTheyWereAdded(): void
    {
        $error1 = $this->createError();
        $error2 = $this->createError();
        $error3 = $this->createError();
        $error4 = $this->createError();

        $sut = new Collection($error1, $error2, $error3, $error4);

        $sut->rewind();
        $this->assertSame($error1, $sut->current());
        $sut->next();
        $this->assertSame($error2, $sut->current());
        $sut->next();
        $this->assertSame($error3, $sut->current());
        $sut->next();
        $this->assertSame($error4, $sut->current());
    }

    public function testIsJsonEncodedProperly(): void
    {
        $error = $this->createError();

        $sut = new Collection($error);

        $result = json_encode($sut);

        $expected = '[{"domain":"foo","message":"bar","reason":"baz"}]';

        $this->assertSame($expected, $result);
    }

    public function testPositionStartsAtZero(): void
    {
        $error = $this->createError();

        $sut = new Collection($error);

        $this->assertSame(0, $sut->key());
    }

    private function createError(): Error
    {
        return new Error('foo', 'bar', 'baz');
    }
}
