<?php

declare(strict_types=1);

namespace ToucandoTests\Middleware\Error\Error;

use PHPUnit\Framework\TestCase;
use Toucando\Middleware\Error\Error\Error;

/**
 * @covers \Toucando\Middleware\Error\Error\Error
 */
final class ErrorTest extends TestCase
{
    public function testDomainPassedToConstructorIsReturned(): void
    {
        $domain = 'foo';

        $sut = new Error($domain, 'bar', 'baz');

        $this->assertSame($domain, $sut->getDomain());
    }

    public function testMessagePassedToConstructorIsReturned(): void
    {
        $message = 'bar';

        $sut = new Error('foo', $message, 'baz');

        $this->assertSame($message, $sut->getMessage());
    }

    public function testReasonPassedToConstructorIsReturned(): void
    {
        $reason = 'baz';

        $sut = new Error('foo', 'bar', $reason);

        $this->assertSame($reason, $sut->getReason());
    }

    public function testIsJsonEncodedProperly(): void
    {
        $sut = new Error('foo', 'bar', 'baz');

        $result = json_encode($sut);

        $expected = '{"domain":"foo","message":"bar","reason":"baz"}';

        $this->assertSame($expected, $result);
    }
}
