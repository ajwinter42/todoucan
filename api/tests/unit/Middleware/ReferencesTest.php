<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Middleware;

use PHPUnit\Framework\TestCase;
use Toucando\Middleware\References;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;

final class ReferencesTest extends TestCase
{
    use Request, Mocks;

    public function testIfBodyContainsValidReferences(): void
    {
        // Given references are in body
        $request = $this->makePostRequest()->withParsedBody([
            'donut-reference'  => 'e17d2e5a-e4b6-41b8-8f25-7d0ad9ce99b7',
            'banana-reference' => '708bd100-1d11-4747-b11a-1037e5687667',
        ]);

        // When request is passed to middleware and is invoked
        $sut = new References('donut-reference', 'banana-reference');

        $result = $sut->process($request, $this->makeRequestHandler());

        // Then all is well
        $this->assertSame(200, $result->getStatusCode());
    }
}
