<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Endpoint;

use PHPUnit\Framework\TestCase;
use Toucando\Endpoint\Authenticate;
use Toucando\Middleware\Error\Exception\InternalError;
use Toucando\Middleware\Error\Exception\Unauthorised;
use Toucando\Persistence\Authentication;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;

final class AuthenticateTest extends TestCase
{
    use Request, Mocks;

    public function testIfUsernameIsNotInBodyReturnUnauthorized(): void
    {
        // Given I have a request without a username
        $request = $this->makePostRequest()->withParsedBody([
            'password' => 'safe password 123',
        ]);

        // When I invoke the middleware
        $sut = new Authenticate(
            $this->mockRespond(),
            $this->mockUserJsonAdapter(),
            $this->mockUuidService(),
            $this->mockEntityManager()
        );

        // An Exception should be thrown
        $this->expectException(Unauthorised::class);

        $sut($request);
    }

    public function testIfPasswordIsNotInBodyReturnUnauthorized(): void
    {
        // Given I have a request without a username
        $request = $this->makePostRequest()->withParsedBody([
            'username' => 'usermcuser',
        ]);

        // When I invoke the middleware
        $sut = new Authenticate(
            $this->mockRespond(),
            $this->mockUserJsonAdapter(),
            $this->mockUuidService(),
            $this->mockEntityManager()
        );

        // An Exception should be thrown
        $this->expectException(Unauthorised::class);

        $sut($request);
    }

    public function testIfNoUsernameIsFoundThenExceptionIsThrown(): void
    {
        $database   = $this->mockEntityManager();
        $repository = $this->mockObjectRepository();
        $username   = 'usermcuser';

        // Given I have a request with a username that does not exist
        $request = $this->makePostRequest()->withParsedBody([
            'username' => $username,
            'password' => 'password 123',
        ]);

        $repository
            ->expects($this->once())
            ->method('findOneBy')
            ->with([
                'username' => $username,
            ])
            ->willReturn(null);

        $database
            ->expects($this->once())
            ->method('getRepository')
            ->with(Authentication::class)
            ->willReturn($repository);

        // When I send the request to the middleware
        $sut = new Authenticate($this->mockRespond(), $this->mockUserJsonAdapter(), $this->mockUuidService(), $database);

        // Then a exception should be thrown when the middleware is invoked
        $this->expectException(Unauthorised::class);

        $sut($request);
    }

    public function testIfPasswordIsInvalidThrowException(): void
    {
        $database   = $this->mockEntityManager();
        $repository = $this->mockObjectRepository();
        $username   = 'usermcuser';

        $entity = $this->mockAuthenticationEntity();

        // Given I have a request that does not contain a valid password
        $request = $this->makePostRequest()->withParsedBody([
            'username' => $username,
            'password' => 'password?',
        ]);

        $entity
            ->expects($this->once())
            ->method('getPassword')
            ->willReturn('real password');

        $repository
            ->expects($this->once())
            ->method('findOneBy')
            ->with([
                'username' => $username,
            ])
            ->willReturn($entity);

        $database
            ->expects($this->once())
            ->method('getRepository')
            ->with(Authentication::class)
            ->willReturn($repository);

        // When I construct the middleware
        $sut = new Authenticate($this->mockRespond(), $this->mockUserJsonAdapter(), $this->mockUuidService(), $database);

        // Then I should get an exception on invoke
        $this->expectException(Unauthorised::class);

        $sut($request);
    }

    public function testIfPersistingTokenFailsInternalErrorIsThrown(): void
    {
        $entity = $this->mockAuthenticationEntity();
        $entity->method('getPassword')->willReturn(password_hash('password', PASSWORD_BCRYPT));

        $repository = $this->mockObjectRepository();
        $repository->method('findOneBy')->willReturn($entity);

        $database = $this->mockEntityManager();
        $database
            ->expects($this->once())
            ->method('persist')
            ->with($entity)
            ->willThrowException(new \Exception());
        $database->method('getRepository')->willReturn($repository);

        // Given I have an instance of the middleware
        $request = $this->makePostRequest()->withParsedBody([
            'username' => 'usermcuser',
            'password' => 'password',
        ]);

        $sut = new Authenticate(
            $this->mockRespond(),
            $this->mockUserJsonAdapter(),
            $this->mockUuidService(),
            $database
        );

        // When the middleware is invoked
        $this->expectException(InternalError::class);
        $sut($request);

        // Then an internal error exception is thrown
    }

    public function testSuccessfulResponseIsReturnedIfUsernameAndPasswordAreCorrect(): void
    {
        $database = $this->mockEntityManager();

        $username = 'alex';
        $password = password_hash('alex123', PASSWORD_BCRYPT);

        $token = $this->mockUuidValue();
        $token
            ->expects($this->once())
            ->method('getRaw')
            ->willReturn('uuid');

        $uuidService = $this->mockUuidService();
        $uuidService
            ->expects($this->once())
            ->method('generate')
            ->willReturn($token);

        $user = $this->mockUserEntity();

        $entity = $this->mockAuthenticationEntity();
        $entity
            ->expects($this->once())
            ->method('getPassword')
            ->willReturn($password);
        $entity
            ->expects($this->once())
            ->method('setCurrentToken')
            ->with($token);
        $entity
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($user);

        $repository = $this->mockObjectRepository();
        $repository
            ->expects($this->once())
            ->method('findOneBy')
            ->with(['username' => $username])
            ->willReturn($entity);

        $database
            ->expects($this->once())
            ->method('getRepository')
            ->with(Authentication::class)
            ->willReturn($repository);

        $database
            ->expects($this->once())
            ->method('persist')
            ->with($entity);

        $database
            ->expects($this->once())
            ->method('flush');

        $respond = $this->mockRespond();
        $respond
            ->expects($this->once())
            ->method('success')
            ->with([
                'token' => 'uuid',
                'user' => [],
            ]);

        $jsonAdapter = $this->mockUserJsonAdapter();
        $jsonAdapter
            ->expects($this->once())
            ->method('toJson')
            ->with($user)
            ->willReturn([]);

        $sut = new Authenticate($respond, $jsonAdapter, $uuidService, $database);

        $request = $this->makePostRequest()->withParsedBody(['username' => $username, 'password' => 'alex123']);

        $sut($request);
    }
}
