<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Endpoint\JobList;

use Exception;
use PHPUnit\Framework\TestCase;
use Toucando\Endpoint\JobList\Create;
use Toucando\Middleware\Error\Exception\BadRequest;
use Toucando\Middleware\Error\Exception\InternalError;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;

final class CreateTest extends TestCase
{
    use Request, Mocks;

    public function testIfNameIsBlankThrowBadRequest(): void
    {
        // Given the request has a blank name
        $request = $this->makePostRequest()->withParsedBody([
            'name' => '',
        ]);

        // When I pass the request to the endpoint
        $sut = new Create(
            $this->mockRespond(),
            $this->mockUuidService(),
            $this->mockJobListRepository(),
            $this->mockJobListJsonAdapter()
        );

        $this->expectException(BadRequest::class);
        $sut($request);

        // Then BadRequest exception has been thrown
    }

    public function testIfNoNameIsInRequestThrowBadRequest(): void
    {
        // Given the request has no name
        $request = $this->makePostRequest();

        // When I pass the request to the endpoint
        $sut = new Create(
            $this->mockRespond(),
            $this->mockUuidService(),
            $this->mockJobListRepository(),
            $this->mockJobListJsonAdapter()
        );

        $this->expectException(BadRequest::class);
        $sut($request);

        // Then BadRequest exception has been thrown
    }

    public function testIfPersistFailsThrowInternalError(): void
    {
        // Given the request contains a valid name
        $request = $this->makePostRequest()
            ->withParsedBody([
                'name' => 'Job list 1',
            ])
            ->withAttribute('logged-in-user', $this->mockUserEntity());

        $jobListRepository = $this->mockJobListRepository();
        $jobListRepository
            ->expects($this->once())
            ->method('persist')
            ->willThrowException(new Exception());

        $uuidService = $this->mockUuidService();
        $uuidService
            ->expects($this->once())
            ->method('generate')
            ->willReturn($this->mockUuidValue());

        // When I pass the request to the endpoint
        $sut = new Create(
            $this->mockRespond(),
            $uuidService,
            $jobListRepository,
            $this->mockJobListJsonAdapter()
        );

        $this->expectException(InternalError::class);
        $sut($request);

        // Internal Error exception is thrown
    }

    public function testIfAllIsWellSuccessResponseIsReturned(): void
    {
        // Given the request contains a valid name
        $request = $this->makePostRequest()
            ->withParsedBody([
                'name' => 'Job list 1',
            ])
            ->withAttribute('logged-in-user', $this->mockUserEntity());

        $jobListRepository = $this->mockJobListRepository();
        $jobListRepository
            ->expects($this->once())
            ->method('persist');

        $uuidService = $this->mockUuidService();
        $uuidService
            ->expects($this->once())
            ->method('generate')
            ->willReturn($this->mockUuidValue());

        $jobListJsonAdapter = $this->mockJobListJsonAdapter();
        $jobListJsonAdapter
            ->expects($this->once())
            ->method('toJson')
            ->willReturn([]);

        $respond = $this->mockRespond();
        $respond
            ->expects($this->once())
            ->method('success')
            ->with(['jobList' => []]);

        // When I pass the request to the endpoint
        $sut = new Create($respond, $uuidService, $jobListRepository, $jobListJsonAdapter);

        $sut($request);

        // Then all is well
    }
}
