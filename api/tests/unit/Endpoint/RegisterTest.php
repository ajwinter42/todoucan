<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Endpoint;

use Doctrine\Common\Persistence\ObjectRepository;
use Exception;
use PHPUnit\Framework\TestCase;
use Toucando\Endpoint\Register;
use Toucando\Middleware\Error\Exception\BadRequest;
use Toucando\Middleware\Error\Exception\InternalError;
use Toucando\Persistence\Authentication;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;
use Zend\Diactoros\Response;

/**
 * @covers \Toucando\Endpoint\Register
 */
final class RegisterTest extends TestCase
{
    use Request, Mocks;

    /**
     * @dataProvider provideFields
     *
     * @param string[] $body
     */
    public function testAllFieldsMustBePassed(array $body, string $exceptionMessage): void
    {
        $sut = new Register(
            $this->mockEntityManager(),
            $this->mockRespond(),
            $this->mockUuidService()
        );

        $request = $this
            ->makePostRequest()
            ->withParsedBody($body);

        $this->expectException(BadRequest::class);
        $this->expectExceptionMessage($exceptionMessage);

        $sut($request);
    }

    public function provideFields(): array
    {
        return [
            [['username' => 'aaa', 'password' => 'aaa'], 'Password confirmation field must be set'],
            [['password' => 'aaa', 'password-confirmation' => 'aaa'], 'Username field must be set'],
            [['username' => 'aaa', 'password-confirmation' => 'aaa'], 'Password field must be set'],
        ];
    }

    public function testIfPasswordAndPasswordConfirmationDoNotMatch(): void
    {
        $sut = new Register(
            $this->mockEntityManager(),
            $this->mockRespond(),
            $this->mockUuidService()
        );

        $request = $this
            ->makePostRequest()
            ->withParsedBody([
                'username'              => 'aaa',
                'password'              => 'aaa',
                'password-confirmation' => 'bbb',
            ]);

        $this->expectException(BadRequest::class);

        $sut($request);
    }

    public function testIfUserNameAlreadyExistsThrowsBadRequestException(): void
    {
        $username = 'aaa';

        $repository = $this->createMock(ObjectRepository::class);
        $repository
            ->expects($this->once())
            ->method('findBy')
            ->with([
                'username' => $username
            ])
            ->willReturn([
                $this->mockAuthenticationEntity()
            ]);

        $entityManager = $this->mockEntityManager();
        $entityManager
            ->expects($this->once())
            ->method('getRepository')
            ->with(Authentication::class)
            ->willReturn($repository);

        $sut = new Register(
            $entityManager,
            $this->mockRespond(),
            $this->mockUuidService()
        );

        $request = $this
            ->makePostRequest()
            ->withParsedBody([
                'username'              => $username,
                'password'              => 'aaa',
                'password-confirmation' => 'aaa',
            ]);

        $this->expectException(BadRequest::class);
        $this->expectExceptionMessage('Username already exists');

        $sut($request);
    }

    public function testIfPersistenceFailsAServerErrorIsThrown(): void
    {
        $username = 'aaa';

        $repository = $this->createMock(ObjectRepository::class);
        $repository
            ->expects($this->once())
            ->method('findBy')
            ->with([
                'username' => $username
            ])
            ->willReturn([]);

        $entityManager = $this->mockEntityManager();
        $entityManager
            ->expects($this->once())
            ->method('getRepository')
            ->with(Authentication::class)
            ->willReturn($repository);

        $entityManager
            ->expects($this->once())
            ->method('persist')
            ->willThrowException(new Exception());

        $uuidService = $this->mockUuidService();
        $uuidService
            ->expects($this->exactly(2))
            ->method('generate')
            ->willReturnOnConsecutiveCalls($this->mockUuidValue(), $this->mockUuidValue());

        $sut = new Register(
            $entityManager,
            $this->mockRespond(),
            $uuidService
        );

        $request = $this
            ->makePostRequest()
            ->withParsedBody([
                'username'              => $username,
                'password'              => 'aaa',
                'password-confirmation' => 'aaa',
            ]);

        $this->expectException(InternalError::class);

        $response = $sut($request);

        $this->assertSame(200, $response->getStatusCode());
    }

    public function testAllFieldsAreSentReturns200Response(): void
    {
        $username = 'aaa';

        $repository = $this->createMock(ObjectRepository::class);
        $repository
            ->expects($this->once())
            ->method('findBy')
            ->with([
                'username' => $username
            ])
            ->willReturn([]);

        $entityManager = $this->mockEntityManager();
        $entityManager
            ->expects($this->once())
            ->method('getRepository')
            ->with(Authentication::class)
            ->willReturn($repository);

        $entityManager
            ->expects($this->exactly(2))
            ->method('persist');

        $entityManager
            ->expects($this->once())
            ->method('flush');

        $response = new Response();
        $response->withStatus(200);

        $respond = $this->mockRespond();
        $respond
            ->expects($this->once())
            ->method('success')
            ->willReturn($response);

        $uuidService = $this->mockUuidService();
        $uuidService
            ->expects($this->exactly(2))
            ->method('generate')
            ->willReturnOnConsecutiveCalls($this->mockUuidValue(), $this->mockUuidValue());

        $sut = new Register(
            $entityManager,
            $respond,
            $uuidService
        );

        $request = $this
            ->makePostRequest()
            ->withParsedBody([
                'username'              => $username,
                'password'              => 'aaa',
                'password-confirmation' => 'aaa',
            ]);

        $response = $sut($request);

        $this->assertSame(200, $response->getStatusCode());
    }
}
