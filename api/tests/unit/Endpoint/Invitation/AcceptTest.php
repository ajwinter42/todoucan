<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Endpoint\Invitation;

use Exception;
use PHPUnit\Framework\TestCase;
use Toucando\Endpoint\Invitation\Accept;
use Toucando\Middleware\Error\Exception\InternalError;
use Toucando\Middleware\Error\Exception\NotFound;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;

final class AcceptTest extends TestCase
{
    use Request, Mocks;

    public function testIfTheInvitationIsNotFoundThenNotFoundExceptionIsThrown(): void
    {
        $reference = $this->mockUuidValue();

        // Given I have a request with a valid reference
        $invitationRepository = $this->mockInvitationRepository();
        $invitationRepository
            ->expects($this->once())
            ->method('fetchByReference')
            ->with($reference)
            ->willReturn(null);

        $request = $this->makePostRequest()->withAttribute('uuid-invitation-reference', $reference);

        // When I pass the request to the endpoint
        $sut = new Accept($this->mockRespond(), $invitationRepository);

        $this->expectException(NotFound::class);
        $sut($request);

        // Then an exception is thrown
    }

    public function testIfPersistingFailsThrowInternalErrorException(): void
    {
        // Given I have a request with a valid reference but the accept will fail
        $invitation = $this->mockInvitationEntity();
        $user = $this->mockUserEntity();

        $invitationRepository = $this->mockInvitationRepository();
        $invitationRepository
            ->expects($this->once())
            ->method('fetchByReference')
            ->willReturn($invitation);
        $invitationRepository
            ->expects($this->once())
            ->method('accept')
            ->with($invitation, $user)
            ->willThrowException(new Exception());
        $request = $this->makePostRequest()
            ->withAttribute('uuid-invitation-reference', $this->mockUuidValue())
            ->withAttribute('logged-in-user', $user);

        // When I pass the request to the endpoint
        $sut = new Accept($this->mockRespond(), $invitationRepository);

        $this->expectException(InternalError::class);
        $sut($request);

        // Then internal error is thrown
    }

    public function testSuccess200ResponseIsReturnedIfAllIsSuccessful(): void
    {
        // Given I have a request with a valid reference
        $invitation = $this->mockInvitationEntity();
        $user = $this->mockUserEntity();

        $invitationRepository = $this->mockInvitationRepository();
        $invitationRepository
            ->expects($this->once())
            ->method('fetchByReference')
            ->willReturn($invitation);
        $invitationRepository
            ->expects($this->once())
            ->method('accept')
            ->with($invitation, $user);

        $respond = $this->mockRespond();
        $respond
            ->expects($this->once())
            ->method('success');

        $request = $this->makePostRequest()
            ->withAttribute('uuid-invitation-reference', $this->mockUuidValue())
            ->withAttribute('logged-in-user', $user);


        // When I pass the request to the endpoint
        $sut = new Accept($respond, $invitationRepository);

        $sut($request);

        // Then the result should be status 200
    }
}
