<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Endpoint\Invitation;

use PHPUnit\Framework\TestCase;
use Toucando\Endpoint\Invitation\Send;
use Toucando\Middleware\Error\Exception\BadRequest;
use Toucando\Middleware\Error\Exception\InternalError;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;

final class SendTest extends TestCase
{
    use Request, Mocks;

    public function testIfNoReferencesAreInTheRequestThenThrowBadRequest(): void
    {
        // Given I have a request that is missing references
        $request = $this->makePostRequest();

        // When the request is sent to the endpoint
        $sut = new Send($this->mockRespond(), $this->mockUserRepository(), $this->mockInvitationRepository());

        $this->expectException(BadRequest::class);
        $sut($request);

        // Then a bad request exception is thrown
    }

    public function testIfReferencesAreInvalidThrowBadRequest(): void
    {
        // Given I have a request with bad references
        $request = $this->makePostRequest()->withParsedBody([
            'references' => [
                'not a uuid',
                'also not a uuid',
            ],
        ]);

        // When I pass the request to the endpoint
        $sut = new Send($this->mockRespond(), $this->mockUserRepository(), $this->mockInvitationRepository());

        $this->expectException(BadRequest::class);
        $sut($request);

        // Then I get a bad request exception
    }

    public function testIfPersistingFailsThenThrowInternalError(): void
    {
        $sender = $this->mockUserEntity();

        $users = [
            $this->mockUserEntity(),
            $this->mockUserEntity(),
        ];

        // Given I have a request that is valid
        $request = $this->makePostRequest()
            ->withParsedBody([
                'references' => [
                    '82c04389-f98f-4075-a194-5af212291c85',
                    'f2e36ffa-da6b-427b-a683-5b89dedbef08',
                ],
                'message' => 'please join my list',
                'item-reference' => '59b506e0-8063-4c73-a0ec-6319bf124bda',
            ])
            ->withAttribute('logged-in-user', $sender);

        $userRepository = $this->mockUserRepository();
        $userRepository->method('fetchByReferences')->willReturn($users);

        $invitationRepository = $this->mockInvitationRepository();
        $invitationRepository->method('send')->willThrowException(new \Exception());

        // When I pass request to endpoint
        $sut = new Send($this->mockRespond(), $userRepository, $invitationRepository);

        $this->expectException(InternalError::class);
        $sut($request);

        // Then a internal error exception has been thrown
    }

    public function testIfValidRequestIsSendThenSuccessResponseIsReturned(): void
    {
        $sender = $this->mockUserEntity();

        $users = [
            $this->mockUserEntity(),
            $this->mockUserEntity(),
        ];

        $message = 'please join my list';

        // Given
        $request = $this->makePostRequest()
            ->withParsedBody([
                'references' => [
                    '82c04389-f98f-4075-a194-5af212291c85',
                    'f2e36ffa-da6b-427b-a683-5b89dedbef08',
                ],
                'message' => $message,
                'item-reference' => 'f2e36ffa-da6b-427b-a683-5b89dedbef09',
            ])
            ->withAttribute('logged-in-user', $sender);

        $userRepository = $this->mockUserRepository();
        $userRepository
            ->expects($this->once())
            ->method('fetchByReferences')
            ->willReturn($users);

        $invitationRepository = $this->mockInvitationRepository();
        $invitationRepository
            ->expects($this->once())
            ->method('send');

        $respond = $this->mockRespond();
        $respond
            ->expects($this->once())
            ->method('success');

        // When I pass the request into the endpoint
        $sut = new Send($respond, $userRepository, $invitationRepository);

        $sut($request);

        // Then the endpoint was successful
    }
}
