<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Endpoint\User;

use PHPUnit\Framework\TestCase;
use Toucando\Endpoint\User\Jobs;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;

/**
 * @covers \Toucando\Endpoint\User\Jobs
 */
final class JobsTest extends TestCase
{
    use Request, Mocks;

    public function testReturnsJobsAssignedAndJobsCreated(): void
    {
        $user = $this->mockUserEntity();

        $jobsAssigned = [$this->mockJobEntity(), $this->mockJobEntity()];
        $jobsCreatedBy = [$this->mockJobEntity(), $this->mockJobEntity()];

        $jobListsAssigned = [$this->mockJobListEntity(), $this->mockJobListEntity()];
        $jobListsCreatedBy = [$this->mockJobListEntity(), $this->mockJobListEntity()];

        $jobsAssignedJson = [['name' => 'Job 1'], ['name' => 'Job 2']];
        $jobsCreatedByJson = [['name' => 'Job 3'], ['name' => 'Job 4']];

        $jobListsAssignedJson = [['name' => 'Job List 1'], ['name' => 'Job List 2']];
        $jobListsCreatedByJson = [['name' => 'Job List 3'], ['name' => 'Job List 4']];

        $jobRepository = $this->mockJobRepository();
        $jobRepository->expects($this->once())->method('fetchUnlistedByAssignee')->with($user)->willReturn($jobsAssigned);
        $jobRepository->expects($this->once())->method('fetchUnlistedByCreatedBy')->with($user)->willReturn($jobsCreatedBy);

        $jobListRepository = $this->mockJobListRepository();
        $jobListRepository->expects($this->once())->method('fetchByAssignee')->with($user)->willReturn($jobListsAssigned);
        $jobListRepository->expects($this->once())->method('fetchByCreatedBy')->with($user)->willReturn($jobListsCreatedBy);

        $jobJsonAdapter = $this->mockJobAdapter();
        $jobJsonAdapter
            ->expects($this->exactly(2))
            ->method('multipleToJson')
            ->withConsecutive(
                $jobsAssigned,
                $jobsCreatedBy
            )
            ->willReturnOnConsecutiveCalls(
                $jobsAssignedJson,
                $jobsCreatedByJson
            );

        $jobListJsonAdapter = $this->mockJobListJsonAdapter();
        $jobListJsonAdapter
            ->expects($this->exactly(2))
            ->method('multipleToJson')
            ->withConsecutive(
                $jobListsAssigned,
                $jobListsCreatedBy
            )
            ->willReturnOnConsecutiveCalls(
                $jobListsAssignedJson,
                $jobListsCreatedByJson
            );

        $respond = $this->mockRespond();
        $respond
            ->expects($this->once())
            ->method('success')
            ->with([
                'jobsAssigned' => $jobsAssignedJson,
                'jobsCreated' => $jobsCreatedByJson,
                'jobListsAssigned' => $jobListsAssignedJson,
                'jobListsCreated' => $jobListsCreatedByJson,
            ]);

        $sut = new Jobs(
            $jobRepository,
            $jobListRepository,
            $jobJsonAdapter,
            $jobListJsonAdapter,
            $respond
        );

        $request = $this->makePostRequest()->withAttribute('logged-in-user', $user);

        $sut($request);
    }
}
