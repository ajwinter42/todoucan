<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Persistence;

use PHPUnit\Framework\TestCase;
use Toucando\Persistence\JobList;
use ToucandoTests\Unit\Mocks;

/**
 * @covers \Toucando\Persistence\JobList
 */
final class JobListTest extends TestCase
{
    use Mocks;

    public function testSetAndRetrieveReference(): void
    {
        // Given I have a reference
        $uuid = 'a26a4289-10ce-4c2f-9bf0-52195ab19fbc';

        $reference = $this->mockUuidValue();
        $reference
            ->expects($this->once())
            ->method('getRaw')
            ->willReturn($uuid);

        // When I pass this reference to the JobList
        $sut = new JobList($reference, $this->mockUserEntity(), '');

        // Then I can retrieve that same reference
        $this->assertSame($uuid, $sut->getReference()->getRaw());
    }

    public function testSetAndRetrieveName(): void
    {
        // Given I have a name
        $name = 'name';

        // When I pass this name to the JobList
        $sut = new JobList($this->mockUuidValue(), $this->mockUserEntity(), $name);

        // Then I can retrieve that same name
        $this->assertSame($name, $sut->getName());
    }

    public function testSetAndRetrieveUser(): void
    {
        // Given I have a name
        $user = $this->mockUserEntity();

        // When I pass this name to the JobList
        $sut = new JobList($this->mockUuidValue(), $this->mockUserEntity(), '');

        $sut->addUser($user);

        // Then I can retrieve that same name
        $this->assertSame($user, $sut->getUsers()[0]);
    }

    public function testSetAndRetrieveJob(): void
    {
        // Given I have a name
        $job = $this->mockJobEntity();

        // When I pass this name to the JobList
        $sut = new JobList($this->mockUuidValue(), $this->mockUserEntity(), '');

        $sut->addJob($job);

        // Then I can retrieve that same name
        $this->assertSame($job, $sut->getJobs()[0]);
    }
}
