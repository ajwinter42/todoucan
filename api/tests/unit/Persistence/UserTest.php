<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Persistence;

use PHPUnit\Framework\TestCase;
use Toucando\Persistence\JobList;
use Toucando\Value\Uuid;
use ToucandoTests\Unit\Mocks;
use Toucando\Persistence\User;

/**
 * @covers \Toucando\Persistence\User
 */
final class UserTest extends TestCase
{
    use Mocks;

    public function testCanSetAndRetrieveReference(): void
    {
        $uuid = 'b876ebce-95e0-4247-b00d-54119c89a8af';

        $reference = $this->mockUuidValue();
        $reference->expects($this->once())->method('getRaw')->willReturn($uuid);

        $sut = new User($reference);

        $this->assertSame($uuid, $sut->getReference()->getRaw());
    }

    public function testUserCanBeAssignedJobList(): void
    {
        $sut = new User(new Uuid('b876ebce-95e0-4247-b00d-54119c89a8af'));

        $jobList = new JobList(
            new Uuid('55547825-8384-4c39-aab5-c7ecc4978712'),
            new User(new Uuid('55547825-8384-4c39-aab5-c7ecc4978712')),
            'Job List A'
        );

        $sut->assignJobList($jobList);

        $this->assertSame($jobList, $sut->getAssignedJobLists()[0]);
    }
}
