<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Traits;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;
use Zend\Diactoros\ServerRequestFactory;

trait Request
{
    public function makePostRequest(): ServerRequestInterface
    {
        $factory = new ServerRequestFactory();

        return $factory->createServerRequest('POST', '/');
    }

    public function makeRequestHandler(): RequestHandlerInterface
    {
        return new class implements RequestHandlerInterface
        {
            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                return new Response();
            }
        };
    }
}
