<?php

declare(strict_types=1);

namespace ToucandoTests\Unit;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Toucando\Persistence\AuthenticationInterface;
use Toucando\Persistence\InvitationInterface;
use Toucando\Persistence\JobListInterface as JobListEntityInterface;
use Toucando\Persistence\UserInterface as UserEntity;
use Toucando\Service\JsonAdapter\JobListInterface as JobListJsonAdapter;
use Toucando\Service\Repository\InvitationInterface as InvitationRepository;
use Toucando\Service\Repository\JobListInterface;
use Toucando\Service\RespondInterface;
use Toucando\Service\UuidInterface;
use Toucando\Value\UuidInterface as UuidValue;
use Toucando\Persistence\JobInterface as JobEntity;
use Toucando\Service\JsonAdapter\JobInterface as JobJsonAdapter;
use Toucando\Service\JsonAdapter\UserInterface as UserJsonAdapter;
use Toucando\Service\Repository\JobInterface as JobRepository;
use Toucando\Service\Repository\UserInterface as UserRepository;

trait Mocks
{
    /**
     * @return UuidValue|MockObject
     */
    private function mockUuidValue(): UuidValue
    {
        return $this->createMock(UuidValue::class);
    }

    /**
     * @return UuidInterface|MockObject
     */
    private function mockUuidService(): UuidInterface
    {
        return $this->createMock(UuidInterface::class);
    }

    /**
     * @return UserEntity|MockObject
     */
    private function mockUserEntity(): UserEntity
    {
        return $this->createMock(UserEntity::class);
    }

    /**
     * @return JobEntity|MockObject
     */
    private function mockJobEntity(): JobEntity
    {
        return $this->createMock(JobEntity::class);
    }

    /**
     * @return EntityManagerInterface|MockObject
     */
    private function mockEntityManager(): EntityManagerInterface
    {
        return $this->createMock(EntityManagerInterface::class);
    }

    /**
     * @return UserRepository|MockObject
     */
    private function mockUserRepository(): UserRepository
    {
        return $this->createMock(UserRepository::class);
    }

    /**
     * @return JobRepository|MockObject
     */
    private function mockJobRepository(): JobRepository
    {
        return $this->createMock(JobRepository::class);
    }

    /**
     * @return UserJsonAdapter|MockObject
     */
    private function mockUserJsonAdapter(): UserJsonAdapter
    {
        return $this->createMock(UserJsonAdapter::class);
    }

    /**
     * @return JobJsonAdapter|MockObject
     */
    private function mockJobAdapter(): JobJsonAdapter
    {
        return $this->createMock(JobJsonAdapter::class);
    }

    /**
     * @return RespondInterface|MockObject
     */
    private function mockRespond(): RespondInterface
    {
        return $this->createMock(RespondInterface::class);
    }

    /**
     * @return AuthenticationInterface|MockObject
     */
    private function mockAuthenticationEntity(): AuthenticationInterface
    {
        return $this->createMock(AuthenticationInterface::class);
    }

    /**
     * @return ObjectRepository|MockObject
     */
    private function mockObjectRepository(): ObjectRepository
    {
        return $this->createMock(ObjectRepository::class);
    }

    /**
     * @return InvitationRepository|MockObject
     */
    private function mockInvitationRepository(): InvitationRepository
    {
        return $this->createMock(InvitationRepository::class);
    }

    /**
     * @return InvitationInterface|MockObject
     */
    private function mockInvitationEntity(): InvitationInterface
    {
        return $this->createMock(InvitationInterface::class);
    }

    /**
     * @return JobListInterface|MockObject
     */
    private function mockJobListRepository(): JobListInterface
    {
        return $this->createMock(JobListInterface::class);
    }

    /**
     * @return JobListJsonAdapter|MockObject
     */
    private function mockJobListJsonAdapter(): JobListJsonAdapter
    {
        return $this->createMock(JobListJsonAdapter::class);
    }

    /**
     * @return ResponseFactoryInterface|MockObject
     */
    private function mockResponseFactory(): ResponseFactoryInterface
    {
        return $this->createMock(ResponseFactoryInterface::class);
    }

    /**
     * @return ResponseInterface|MockObject
     */
    private function mockResponse(): ResponseInterface
    {
        return $this->createMock(ResponseInterface::class);
    }

    /**
     * @return StreamInterface|MockObject
     */
    private function mockStream(): StreamInterface
    {
        return $this->createMock(StreamInterface::class);
    }

    /**
     * @return JobListEntityInterface|MockObject
     */
    private function mockJobListEntity(): JobListEntityInterface
    {
        return $this->createMock(JobListEntityInterface::class);
    }
}
