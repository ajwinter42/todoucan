<?php

declare(strict_types=1);

namespace ToucandoTests\Unit;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Pimple\Container as PimpleContainer;
use Toucando\Container;

/**
 * @covers \Toucando\Container
 */
final class ContainerTest extends TestCase
{
    public function testCanStoreValuesAgainstIds(): void
    {
        $pimple = $this->mockPimple();
        $pimple
            ->expects($this->once())
            ->method('offsetGet')
            ->with('dogSound')
            ->willReturn('woof woof');

        // Given I have a value I would like to save in the container
        $value = 'woof woof';

        // When I store it in the container
        $sut = new Container($pimple);

        // Then I should be able to retrieve that value out of the container
        $this->assertSame($value, $sut->dogSound);
    }

    /**
     * @return PimpleContainer|MockObject
     */
    private function mockPimple(): PimpleContainer
    {
        return $this->createMock(PimpleContainer::class);
    }
}
