#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

docker-compose up -d

docker-compose exec app composer db:migrate
docker-compose exec app composer tests:unit
docker-compose exec app composer tests:system
